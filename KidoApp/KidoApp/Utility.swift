//
//  Utility.swift
//  KidoApp
//
//  Created by monika kumari on 12/06/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit


public enum UIUserInterfaceIdiom : Int {

    case unspecified

    case phone // iPhone and iPod touch style UI

    case pad // iPad style UI

    @available(iOS 9.0, *)
    case tv // Apple TV style UI

    @available(iOS 9.0, *)
    case carPlay // CarPlay style UI
}


enum AlertTypeEnum:Int
{
    case onebutton       =  1
    case twobutton       = 2
}
enum rhymesstorybackgroundImage:String
{
    case rhymes1BG          = "flowerplant"
    case rhymes1Mary          = "mary"
    case rhymes1waterSpayer        = "watersprayer"
    case rhymes2        = "sea1"
    case story1BG          = "desert"
    case story1crow          = "crow"
    case story1pot          = "pot"
    case story2Rabbit  = "rabbit"
    case story2lion  = "lion"
    case sun  = "sun"
    case ship          = "ship"
    case fish          = "fish"
    case story2BG   = "tree"
    case Rhymes2BG          = "mountain"
    case Rhymes5Boy         = "rainyboy"
    case Rhymes4Johny        = "johny"
    case Rhymes4papa      = "papa"
    case Rhymes4sugar      = "sugar"

}

enum Imagetag:Int
{
    case crow = 0
     case pot = 1
     case lion = 2
    case story1bg = 5
    case story2bg = 6
    case ship = 7
    case swinganimation = 2222

    case waterspayer = 9
    case flower = 10
    case fish = 11
    case papa = 12
    case johny = 13
    case sugar = 14
    case FlashAnimation = 15


  
}
enum animationViewFile:String
{
    case boat        = "boat"
    case background           = "background"
     case emoji        = "emoji"
     case welcome          = "welcome"
     case kidslogo  = "kids-logo"
    case hello  = "hello"
    case plant  = "flower"
    case merry  = "merry"
    case bell  = "bell-notification"
    case fish  = "catch-fish"
    case heart  = "heart-boom"
    case plasticbag  = "plastic-bag"
    case cat        = "cat"




}
enum learnListType:Int{
    case learn           = 0
    case play           = 1
    case draw           = 2


}
enum RhymeListType:Int{
    case babablacksheep           = 0
    case bingo           = 1
    case insywinsyspider           = 2
    case rowyourboat           = 3


}
enum gameListType:Int{
    case feedcrocodile           = 0
    case tictactoe           = 1

}
enum drawListItems:String
{
    case picture           = "pictures"
     case cards        = "cards"
     case slideshow          = "slideshow"
   

}
enum learnListItems:String
{
    case story           = "STORIES"
     case rhymes        = "RHYMES"
     case color          = "COLORS"
     case shapes  = "SHAPES"
    case animals           = "ANIMALS"
    case birds        = "BIRDS"
    case instruments          = "INSTRUMENTS"
    case alphabets  = "ALPHABETS"
    case numbers  = "NUMBERS"

}
enum audiofilename:String
{
    case buttonclicked  = "buttonClicked"
    case jump           = "jump"
    case beep        = "beep"
    case laugh          = "laugh"
    case glass  = "glass"
    case explosion  = "explosion"
    case monkey  = "monkey"
    case clap  = "clap"
    case pig  = "pig"
    case rooster  = "rooster"
    case cat  = "cat"
    case cow  = "cow"
    case rat  = "rat"
    case horse  = "horse"
    case dog  = "dog"
    case lion  = "lion"
    case jail_cell_door  = "jail_cell_door"
   case flip  = "flip"
    


  
}
enum Play_Type:String
{
    case tictocktoe           = "tictocktoe"
    case bubbleshooter        = "bubbleshooter"
    case instrumentsplayer          = "instrumentsplayer"
  
  
}
struct StoryTitle {
       static  let story1          = "A thirsty crow"
    static  let story2          = "Lion and Hare"

  

}
struct RhymeTitle {
       static  let Rhyme1          = "BABA BLACK SHEEP"
    static  let Rhyme2          = "TWINKLE TWINKLE LITTLE STAR"

    static  let Rhyme3          = "INSY WINSY SPIDER"

    static  let Rhyme4          = "ROW ROW YOUR BOAT"


}
struct alertMessage {
       static  let knoWifiMessage          = "Check your Wi-Fi or network connection and try again"
}
struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}
struct animationtypeName {
                static let SlideLeft    = "slideLeft"
               static let SlideRight    = "slideRight"
               static let SlideDown     = "slideDown"
               static let SlideUp       = "slideUp"
               static let SqueezeLeft    = "squeezeLeft"
               static let SqueezeRight   = "squeezeRight"
               static let SqueezeDown    = "squeezeDown"
               static let SqueezeUp    = "squeezeUp"
               static let FadeIn    = "fadeIn"
               static let FadeOut    = "fadeOut"
               static let FadeOutIn    = "fadeOutIn"
                
               static let FadeInLeft    = "fadeInLeft"
              static let FadeInRight    = "fadeInRight"
                 
               static let FadeInDown    = "fadeInDown"
                 
               static let FadeInUp    = "fadeInUp"
             
               static let ZoomIn    = "zoomIn"
                   
               static let ZoomOut    = "zoomOut"
                   
               static let Fall    = "fall"
                 
               static let Shake    = "shake"
                 
               static let Pop    = "pop"

               static let FlipX    = "flipX"
                  
               static let FlipY    = "flipY"
                 
               static let Morph    = "morph"
                   
               static let Squeeze    = "squeeze"
                  
               static let Flash    = "flash"
                 
               static let Wobble    = "wobble"
               
               static let Swing    = "swing"
}
struct BaseFont {
    //ionicons_B
    
    static let baseFontIonicons_B = "ionicons"
    static let baseFontIonicons_B2 = "icomoon"

    static let baseFontRobotoSemibold = "Roboto-Semibold"
    static let HelveticaNeueFont = "HelveticaNeue"
   // static let baseFontRobotoHairline = "Roboto-Hairline"
   // static let baseFontRobotoThin = "Roboto-Thin"
   // static let baseFontRobotoHairlineItalic = "Roboto-HairlineItalic"
    static let baseFontRobotoThin = "Roboto-Thin"
   // static let baseFontRobotoThinItalic = "Roboto-ThinItalic"
   // static let baseFontRobotoLightItalic = "Roboto-LightItalic"
   // static let baseFontRobotoItalic = "Roboto-Italic"
    static let HelveticaNeueBoldFont = "HelveticaNeue-Bold"
   // static let baseFontRobotoSemiboldItalic = "Roboto-SemiboldItalic"
   // static let HelveticaNeueBoldFontItalic = "Roboto-BoldItalic"
   // static let baseFontRobotoMediumItalic = "Roboto-MediumItalic"
   // static let baseFontRobotoBlack = "Roboto-Black"
   // static let baseFontRobotoHeavyItalic = "Roboto-HeavyItalic"
    static let baseFontRobotoLight = "Roboto-Light"
   // static let baseFontRobotoBlackItalic = "Roboto-BlackItalic"
   // static let baseFontRobotoHeavy = "Roboto-Heavy"
    static let baseFontBRLNSR = "BerlinSansFB-Reg"

    
}
struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPHONE_6_OR_More          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH > 568.0
    
}
struct iConStr
{
    static let strEmail_icon:String =  "\u{f2eb}"
}
class Utility {
    class func getOrangeColor()-> UIColor {
        return  UIColor(red:237.0/255.0, green: 130.0/255.0, blue: 34.0/255.0, alpha: 1.0)
    }
    class LableFont:  UILabel {
          required init?(coder aDecoder: NSCoder) {
              super.init(coder: aDecoder)
              self.font  =  UIFont(name: BaseFont.HelveticaNeueFont, size: (self.font.pointSize)*Utility.getScreenScaleFactor())
          }
          override init(frame: CGRect) {
              super.init(frame: frame)
          }
      }
      
      
      class ButtonFont:  UIButton {
          
          internal var hasSelected: Bool = false
          
          required init?(coder aDecoder: NSCoder) {
              super.init(coder: aDecoder)
              self.titleLabel?.font = UIFont(name: BaseFont.HelveticaNeueFont, size: (self.titleLabel?.font.pointSize)!*Utility.getScreenScaleFactor())
          }
      }
      
      class UITextFieldFont:  UITextField {
          required init?(coder aDecoder: NSCoder) {
              super.init(coder: aDecoder)
              self.font = UIFont(name: (BaseFont.HelveticaNeueFont), size: (self.font!.pointSize) * Utility.getScreenScaleFactor())
          }
      }
      
      //MARK: - Get Screen Scale Factor
        
         
      
      //MARK: - Get Screen Scale Factor
      class func getScreenScaleFactor() -> CGFloat{
          let orientation:   UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        var factor :CGFloat
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            factor = 320.0
        }
        else{
            factor = 768.0
        }
        if orientation.isPortrait {
              return UIScreen.main.bounds.size.width / factor
          } else {
              return ScreenSize.SCREEN_HEIGHT / factor
          }
      }
    
      //MARK: - Base font methods
      //baseFontIonicons_B
      class func baseFontIonicons_BSize(size:CGFloat)-> UIFont {
      
          return UIFont(name: BaseFont.baseFontIonicons_B, size: size)!
      }
      class func baseFontIonicons_BSize2(size:CGFloat)-> UIFont {
          
          return UIFont(name: BaseFont.baseFontIonicons_B2, size: size)!
      }
      class func fontWithSize(size:CGFloat)-> UIFont {
          
          return UIFont(name: BaseFont.HelveticaNeueBoldFont, size: size)!
      }
      class func fontBRLNSRWithSize(size:CGFloat)-> UIFont {
          
          return UIFont(name: BaseFont.baseFontBRLNSR, size: size)!
      }
     class func fontBoldWithSize(size:CGFloat)-> UIFont {
          
          return UIFont(name: BaseFont.HelveticaNeueBoldFont, size: size)!
      }
      class func fontThinWithSize(size:CGFloat)-> UIFont {
          
          return UIFont(name: BaseFont.baseFontRobotoLight, size: size)!
      }
    
      
      class func getDefaultGrayColor()-> UIColor {
          return  UIColor(red: 74.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
          
      }
      
      class func getGrayBorderColor()-> UIColor {
          return  UIColor(red: 155.0/255.0, green: 155.0/255.0, blue: 155.0/255.0, alpha: 1.0)
      }
      class func getEmailGrayBorderColor()-> CGColor {
          return  UIColor(red: 137.0/255.0, green: 152.0/255.0, blue: 161.0/255.0, alpha: 1.0).cgColor
      }
      
      class func getGreenBorderColor()-> UIColor {
          return  UIColor(red: 28.0/255.0, green: 177.0/255.0, blue: 37.0/255.0, alpha: 1.0)
      }
      
      class func getBlueBorderColor()-> UIColor {
          return  UIColor(red: 42.0/255.0, green: 147.0/255.0, blue: 153.0/255.0, alpha: 1.0)
      }
      
      class func getSelectedButtonColor()-> UIColor {
          return  UIColor(red: 34.0/255.0, green: 56.0/255.0, blue: 72.0/255.0, alpha: 1.0)
          
      }
    // alert customization
    
    class func attributedTitleForAlert(alert:UIAlertController,message:String) {
        let attributedString = NSAttributedString(string: message, attributes: [
            NSAttributedString.Key.font : Utility.fontWithSize(size: 50.0),
            NSAttributedString.Key.foregroundColor :UIColor.black
            ])
        alert.setValue(attributedString, forKey: "attributedTitle")
    }
    
    class func showAlert(message: String, title: String = "")
        {
            let alertView = UIAlertController(title: title, message: "", preferredStyle: .alert)
            
            Utility.attributedTitleForAlert(alert: alertView, message: message)
            
            alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            
                UIApplication.shared.keyWindow?.rootViewController?.present(alertView, animated: true, completion: nil)
        }
        class func showAlertOnController(message: String, title: String = "",controller:UIViewController)
        {
            let alertView = UIAlertController(title: title, message: "", preferredStyle: .alert)
            
            Utility.attributedTitleForAlert(alert: alertView, message: message)
            
            alertView.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            
            controller.present(alertView, animated: true, completion: nil)
        }
          class func showAlertControllerWith(title:String, message:String?, onVc:UIViewController , style: UIAlertController.Style = .alert, buttons:[String], completion:((Bool,Int)->Void)?) -> Void {

                 let alertController = UIAlertController.init(title: title, message: message, preferredStyle: style)
                 for (index,title) in buttons.enumerated() {
                     let action = UIAlertAction.init(title: title, style: UIAlertAction.Style.default) { (action) in
                         completion?(true,index)
                     }
                     alertController.addAction(action)
                 }

                 onVc.present(alertController, animated: true, completion: nil)
             }

        class func showNetworkAlert(viewController : UIViewController) {
            
            _ = UIAlertController(title: "Network Error", message: "", preferredStyle: .alert)
    //        Utility.attributedTitleForAlert(alert: networkAlert, message: alertMessage.knoWifiMessage)
//            Utility.ShowAlertViewWithAction(message: alertMessage.knoWifiMessage, buttonCount: 1, ONVC: viewController, AlertTag: 0,isSuccess:false)

    //        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
    //        networkAlert.addAction(action)
    //
    //        viewController.present(networkAlert, animated: true, completion: nil)
        }
        
        class func getAppDelegate() -> AppDelegate {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            return appDelegate
        }
        //take the sceenshot of previous view
        class func captureScreen(view:UIView)->UIImage {
            UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.main.scale)
            view.layer.render(in: UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image!
        }
    class func getLightGreenColor()-> UIColor {
           return  UIColor(red: 1.0/255.0, green: 223.0/255.0, blue: 1.0/255.0, alpha: 1.0)
       }
}
