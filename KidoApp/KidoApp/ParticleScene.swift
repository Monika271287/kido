//
//  ParticleScene.swift
//  KidoApp
//
//  Created by monika kumari on 17/06/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//
import Foundation
import SpriteKit

class ParticleScene : SKScene
{
    private var emitter: SKEmitterNode!
    private var presentingView: SKView!

    override func didMove(to view: SKView) {
        scaleMode = .resizeFill

        if let color = view.backgroundColor {
            backgroundColor = color
        }

        if let filter = CIFilter(name: "CIVignetteEffect") {
            filter.setDefaults()
            filter.setValue(CIVector(cgPoint: view.center), forKey: "inputCenter")
            filter.setValue(view.frame.size.width, forKey: "inputRadius")

            self.filter = filter
            self.shouldEnableEffects = true
        }

        presentingView = view
    }
//       private func addRain() {
//              let skView = SKView(frame: view.frame)
//              skView.backgroundColor = .clear
//              let scene = SKScene(size: view.frame.size)
//              scene.backgroundColor = .clear
//              skView.presentScene(scene)
//              skView.isUserInteractionEnabled = false
//        scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)
//              scene.addChild(emitterNode)
//    //          emitterNode.position.y = scene.frame.maxY
//    //          emitterNode.particlePositionRange.dx = scene.frame.width
//              view.addSubview(skView)
//          }
    func startEmission() {
        emitter = SKEmitterNode(fileNamed: "Snow.sks")
        emitter.particlePositionRange = CGVector(dx: presentingView.bounds.size.width, dy: 0)
        emitter.position = CGPoint(x: presentingView.center.x, y: presentingView.bounds.size.height)
        emitter.targetNode = self

        addChild(emitter)
    }

    func tiltParticles(rotation: Double) {
        emitter.xAcceleration = CGFloat(rotation)
    }
}
