//
//  LearnListViewController.swift
//  KidoApp
//
//  Created by monika kumari on 26/07/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import JTMaterialTransition
class ListCollectionViewCell: UICollectionViewCell {

    @IBOutlet var clickableButton: RoundButton!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var ImgV: UIImageView!

}
protocol LEARNLISTControllerDelegate:class {
    func popoverContentFORLearnlist(controller:UIViewController, didselectItem name:String,type:learnListType)
}
class LearnListViewController: BaseViewController,UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout,GameViewControllerDelegate {
   
    
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var itemsArray = [String]()
    @IBOutlet weak var collectionV: UICollectionView!
    var selectedIndex = Int()
    var type = learnListType(rawValue: 0)

    var selectedItems = String()
    @IBOutlet weak var closeButton: UIButton!
    var delegate:LEARNLISTControllerDelegate? //declare a delegate

    @IBAction func closeButtonClicked(_ sender: UIButton) {
               self.navigationController?.popViewController(animated: true)

           }
   
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        if type == learnListType.learn
        {
            itemsArray = ["STORIES", "RHYMES", "COLORS", "SHAPES","ANIMALS","BIRDS","NUMBERS","ALPHABETS"]
        }
        else if type == learnListType.play{
            itemsArray = ["FEED CROCODILE", "Tic Tac Toe"]
        }
       else if type == learnListType.draw
               {
                   itemsArray = ["pictures", "cards", "slideshow", ]
               }
        closeButton.setBackgroundImage(UIImage(named: "back")?.maskWithColor(color: UIColor.green), for: .normal)
        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])
        self.collectionV.backgroundColor = UIColor.clear

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - UICollectionViewDataSource protocol
       func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
          return 1     //return number of sections in collection view
      }
    // tell the collection view how many cells to make
          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return itemsArray.count
          }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! ListCollectionViewCell
        if type == learnListType.draw{
            switch indexPath.row {
            case 0:
                 self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: .photoVideo, fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)
                case 1:
                self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: .birthdayCake, fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)
                case 2:
                self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: .video, fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

            default:
                debugPrint("")
            }
            cell.NameLabel.text = itemsArray[indexPath.row]
        }
        else{
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        switch indexPath.row {
        case 0:
            self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.book :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)
                   cell.NameLabel.text = itemsArray[indexPath.row]
            case 1:
            self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.copy :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)
                  cell.NameLabel.text = itemsArray[indexPath.row]
            
            case 2:
            self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.penSquare :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

                  cell.NameLabel.text = itemsArray[indexPath.row]
            case 3:
          
            self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.shapes :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

                  cell.NameLabel.text = itemsArray[indexPath.row]
            case 4:
                 
                       self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.cat :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

                             cell.NameLabel.text = itemsArray[indexPath.row]
            case 5:
                      self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.kiwiBird :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

                             cell.NameLabel.text = itemsArray[indexPath.row]
            case 6:
                      self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.sortNumericDown :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

                             cell.NameLabel.text = itemsArray[indexPath.row]
            case 7:
                       self.setupfontAwesomeIconToButton(button: cell.clickableButton, iconname: (type == learnListType.learn ?.sortAlphaDown :.gamepad), fontsize:(30 * Utility.getScreenScaleFactor()),color:.black)

                             cell.NameLabel.text = itemsArray[indexPath.row]
            
        default:
             let image = UIImage.fontAwesomeIcon(name: .history, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
                   cell.ImgV.image = image
                   cell.NameLabel.text = itemsArray[indexPath.row]
        }
   
        }
        cell.backgroundColor = UIColor.clear // make cell more visible in our example project

        return cell
    }
    // MARK: - UICollectionViewDelegate protocol

           func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
                          // handle tap events
            self.dismiss(animated: true, completion: {
                self.buttonClickedPlayer?.play()
                let cell  = collectionView.cellForItem(at: indexPath) as! ListCollectionViewCell

               if self.type == learnListType.learn{
//                                           self.delegate?.popoverContentFORLearnlist(controller: self, didselectItem: self.itemsArray[indexPath.row],type: learnListType.learn)
                    let name:String = self.itemsArray[indexPath.row]
                    switch name {
                    case learnListItems.story.rawValue:
                        self.moveToStoryRhymes(selectedIndex: 0, button: cell.clickableButton, selectedItems: name)
                        
                            case learnListItems.rhymes.rawValue:
                            self.moveToStoryRhymes(selectedIndex: 0, button: cell.clickableButton, selectedItems: name)

                    case learnListItems.color.rawValue:
                            self.moveToCircularVC(selectedItemName: name)
                        
                    case learnListItems.shapes.rawValue:
                        self.moveToCircularVC(selectedItemName: name)
                        
                        
                        case learnListItems.animals.rawValue:
                                   self.moveToListVC(selectedItemName: name)
                                   
                         case learnListItems.birds.rawValue:
                                       self.moveToListVC(selectedItemName: name)
                                   
                          case learnListItems.numbers.rawValue:
                                       self.moveToListVC(selectedItemName: name)
                                   
                          case learnListItems.alphabets.rawValue:
                                   self.moveToListVC(selectedItemName: name)

                        default:
                                        dLog(message: "")

                    }
                                    }
               else if self.type == learnListType.draw{
                let name:String = self.itemsArray[indexPath.row]
                                   switch name {
                                   case drawListItems.picture.rawValue:
                                       self.moveTodrawingVC(selectedIndex: 0, button: cell.clickableButton, selectedItems: name)
                                       
                                   case drawListItems.cards.rawValue:
                                           self.moveTodrawingVC(selectedIndex: 0, button: cell.clickableButton, selectedItems: name)

                                   case drawListItems.slideshow.rawValue:
                                           self.moveToSlideShowVC(selectedIndex: 0, button: cell.clickableButton, selectedItems: name)
                                       
                                
                                       default:
                                                       dLog(message: "")

                                   }
               }
                                        else{
                    guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else { return  }
                    self.transition = JTMaterialTransition(animatedView: (cell.clickableButton))
                    
                                        vc.modalPresentationStyle = .custom
                                                  vc.transitioningDelegate = self.transition
                                                         
                                                  self.present(vc, animated: true, completion: nil)

                                        //   self.delegate?.popoverContentFORLearnlist(controller: self, didselectItem: self.itemsArray[indexPath.row],type: learnListType.play)
                   
                     
                   
                                    }

            })
           
            
            

                          
                      }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let screenWidth = UIScreen.main.bounds.width
              let scaleFactor = (screenWidth/2)
              
              return CGSize(width: scaleFactor, height: 240)
          }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
    //MARK:-
    func movetoLearnlistFromGame(game: Int,tocontroller:UIViewController) {
          
      }
}
