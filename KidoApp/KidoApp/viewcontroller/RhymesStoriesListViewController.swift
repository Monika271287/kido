
//
//  RhymesStoriesListViewController.swift
//  KidoApp
//
//  Created by monika kumari on 29/07/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
class RhymeslistViewCell: UICollectionViewCell {

    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var ImgV: UIImageView!

}
class RhymesStoriesListViewController:BaseViewController,UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout {
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var itemsArray = [String]()
    @IBOutlet weak var collectionV: UICollectionView!
    var selectedItems = String()
    var delegate:LEARNLISTControllerDelegate? //declare a delegate

   
   
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        self.navigationController?.navigationBar.isHidden = false
       


        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])
        self.collectionV.backgroundColor = UIColor.clear
        switch selectedItems {
        case learnListItems.story.rawValue:
            itemsArray = ["Story 1","Story 2"]
            case learnListItems.rhymes.rawValue:
                      itemsArray = ["Rhyme 1","Rhyme 2","Rhyme 3","Rhyme 4","Rhyme 5"]
        default:
               print("")

        }

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - UICollectionViewDataSource protocol
       func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
          return 1     //return number of sections in collection view
      }
    // tell the collection view how many cells to make
          func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return itemsArray.count
          }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! RhymeslistViewCell

        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        switch selectedItems {
              case learnListItems.story.rawValue:
                   switch indexPath.row {
                                 case 0:
                              let image = UIImage.fontAwesomeIcon(name: .book, style: .solid, textColor:         UIColor.yellow, size: CGSize(width: 30, height: 30))
                                    cell.ImgV.image = image
                                    cell.NameLabel.text = itemsArray[indexPath.row]
                                 case 1:
                             let image = UIImage.fontAwesomeIcon(name: .book, style: .solid, textColor: UIColor.blue, size: CGSize(width: 40, height: 40))
                                   cell.ImgV.image = image
                                   cell.NameLabel.text = itemsArray[indexPath.row]
                             
                             
                         default:
                              let image = UIImage.fontAwesomeIcon(name: .history, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
                                    cell.ImgV.image = image
                                    cell.NameLabel.text = itemsArray[indexPath.row]
                         }
                    
                  case learnListItems.rhymes.rawValue:
                             switch indexPath.row {
                                           case 0:
                                        let image = UIImage.fontAwesomeIcon(name: .copy, style: .solid, textColor:UIColor.red, size: CGSize(width: 30, height: 30))
                                              cell.ImgV.image = image
                                              cell.NameLabel.text = itemsArray[indexPath.row]
                                           case 1:
                                       let image = UIImage.fontAwesomeIcon(name: .copy, style: .solid, textColor: UIColor.yellow, size: CGSize(width: 40, height: 40))
                                             cell.ImgV.image = image
                                             cell.NameLabel.text = itemsArray[indexPath.row]
                                case 2:
                                let image = UIImage.fontAwesomeIcon(name: .copy, style: .solid, textColor: UIColor.green, size: CGSize(width: 40, height: 40))
                                      cell.ImgV.image = image
                                      cell.NameLabel.text = itemsArray[indexPath.row]
                                case 3:
                                let image = UIImage.fontAwesomeIcon(name: .copy, style: .solid, textColor: UIColor.blue, size: CGSize(width: 40, height: 40))
                                      cell.ImgV.image = image
                                      cell.NameLabel.text = itemsArray[indexPath.row]
                                
                                       case 4:
                                                                      let image = UIImage.fontAwesomeIcon(name: .copy, style: .solid, textColor: UIColor.brown, size: CGSize(width: 40, height: 40))
                                                                            cell.ImgV.image = image
                                                                            cell.NameLabel.text = itemsArray[indexPath.row]
                                                                             
                                       
                                   default:
                                        let image = UIImage.fontAwesomeIcon(name: .history, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
                                              cell.ImgV.image = image
                                              cell.NameLabel.text = itemsArray[indexPath.row]
                                   }
                              
              default:
                      print("")

              }
       
      
        cell.backgroundColor = UIColor.clear // make cell more visible in our example project

        return cell
    }
    // MARK: - UICollectionViewDelegate protocol

           func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            self.buttonClickedPlayer?.play()

                          // handle tap events
//            self.moveToStoryRhymes(selectedIndex: indexPath.row)
                       
                             
                          
                      }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let screenWidth = UIScreen.main.bounds.width
              let scaleFactor = (screenWidth/2)
              
              return CGSize(width: scaleFactor, height: 240)
          }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
}
