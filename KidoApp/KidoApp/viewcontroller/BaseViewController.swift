//
//  BaseViewController.swift
//  KidoApp
//
//  Created by monika kumari on 29/05/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import AVFoundation
import Lottie
import FontAwesome_swift
import YXWaveView

import  JTMaterialTransition


@IBDesignable class RoundedButton: UIButton
{
    override func layoutSubviews() {
        super.layoutSubviews()

        updateCornerRadius()
    }

    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            updateCornerRadius()
        }
    }

    func updateCornerRadius() {
        layer.cornerRadius = rounded ? cornerRadius : 0
        layer.masksToBounds = rounded ? true : false
    }
}
extension NSLayoutConstraint {
    /**
     Change multiplier constraint

     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
    */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {

        NSLayoutConstraint.deactivate([self])

        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

extension UIButton {


    func flash() {
    let flash = CABasicAnimation(keyPath: "opacity")
    flash.duration = 0.3
    flash.fromValue = 1
    flash.toValue = 0.1
    flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    flash.autoreverses = true
    flash.repeatCount = 2
    layer.add(flash, forKey: nil)
    }
}

class BaseViewController: UIViewController,AlertViewControllerDelegate {
    var imgV: UIImageView!
    var transition: JTMaterialTransition?

    func YESAction(_ sender: UIButton) {
        
    }
    
    func NOAction(_ sender: UIButton) {
        
    }
    
    let kSuccessTitle = "Congratulations"
    let kErrorTitle = "Connection error"
    let kNoticeTitle = "Notice"
    let kWarningTitle = "Warning"
    let kInfoTitle = "Info"
    let kSubtitle = "You've just displayed this awesome Pop Up View"
    let kDefaultAnimationDuration = 2.0

    var animationView_background: AnimationView? = .init(name: animationViewFile.background.rawValue)
    var waterView: YXWaveView?
    var centreImgV: UIImageView!


     var buttonClickedPlayer: AVAudioPlayer?

   let particleEmitter = CAEmitterLayer()
    let speechSynthesizer = AVSpeechSynthesizer()

        var dynamicAnimator   : UIDynamicAnimator!
          var gravityBehavior   : UIGravityBehavior!
          var collisionBehavior : UICollisionBehavior!
          var bouncingBehavior  : UIDynamicItemBehavior!
   
//    func addPopInfinityAnimationTo(imageV:SpringImageView)  {
//
//           imageV.repeatCount =  .infinity
//        imageV.animation = animationtypeName.Pop
//        imageV.velocity = 0.1
//        imageV.damping = 0.1
//           imageV.animate()
//
//       }
//    func addSwingInfinityAnimationTo(imageV:SpringImageView)  {
//
//        imageV.repeatCount =  .infinity
//        imageV.animation = animationtypeName.Swing
//        imageV.velocity = 1
//        imageV.damping = 5.0
//        imageV.animate()
//
//    }
//    func addShakeInfinityAnimationTo(imageV:SpringImageView)  {
//
//           imageV.repeatCount =  .infinity
//           imageV.animation = animationtypeName.Shake
//           imageV.velocity = 1
//           imageV.damping = 5.0
//           imageV.animate()
//
//       }
//    func addFlashInfinityAnimationTo(imageV:SpringImageView)  {
//
//           imageV.repeatCount =  .infinity
//           imageV.animation = animationtypeName.Flash
//           imageV.velocity = 1
//           imageV.damping = 5.0
//           imageV.animate()
//
//       }
//
//       func addImageToInnerV(imageName:String,containerV:UIView,frame_:CGRect,tag:Int)  {
//           imgV = SpringImageView(frame: frame_)
//           imgV.image = UIImage(named: imageName)
//           imgV.backgroundColor = UIColor.clear
//           imgV.contentMode = .scaleAspectFit
//
//        imgV.tag = tag
//
//
//           containerV.addSubview(imgV)
//
//           self.view.bringSubviewToFront(self.imgV)
//        if tag == Imagetag.swinganimation.rawValue  {
//                self.addSwingInfinityAnimationTo(imageV:imgV)
//               }
//
//
//
//
//
//       }
      
    func createPlayer(from filename: String) -> AVAudioPlayer? {
      guard let url = Bundle.main.url(
        forResource: filename,
        withExtension: "mp3"
        ) else {
          return nil
      }
      var player = AVAudioPlayer()

      do {
        try player = AVAudioPlayer(contentsOf: url)
        player.prepareToPlay()
      } catch {
        print("Error loading \(url.absoluteString): \(error)")
      }

      return player
    }
    override func viewDidDisappear(_ animated: Bool) {
      NotificationCenter.default.removeObserver(self, name: Notification.Name.NSExtensionHostWillEnterForeground, object: nil)
      }
    @objc func applicationWillEnterForeground(_ notification: NSNotification) {
                         self.animationView_background!.play()
        print("\(self.view.subviews)")
       
        
        
    
    }
    override func viewDidAppear(_ animated: Bool) {
             super.viewDidAppear(animated)
       
               NotificationCenter.default.addObserver(
               self,
               selector: #selector(applicationWillEnterForeground(_:)),
               name: UIApplication.willEnterForegroundNotification,
               object: nil)

             
             
           }
    
    func addwaterWave(imageName:String,innerV_:UIView,frame_:CGRect,tag:Int)  {
       // centreImgV = UIImageView(frame: CGRect(x: ((ScreenSize.SCREEN_WIDTH)-width)/2, y: height + 50, width: width, height: height))

        let avatarFrame = CGRect(x: 0, y: 0, width: 60, height: 60)
        let avatarView = UIImageView(frame: avatarFrame)
        avatarView.layer.cornerRadius = avatarView.bounds.height/2
        avatarView.layer.masksToBounds = true
        avatarView.layer.borderColor  = UIColor.white.cgColor
        avatarView.layer.borderWidth = 3
        avatarView.layer.contents = UIImage(named: imageName)?.cgImage

        let frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 200)
        waterView = YXWaveView(frame: frame, color: UIColor.white)
        // real wave color
        waterView?.realWaveColor = UIColor.blue

        // mask wave color
        waterView?.maskWaveColor = UIColor.black

        // wave speed (default: 0.6)
        waterView?.waveSpeed = 0.5

        // wave height (default: 5)
        waterView?.waveHeight = ScreenSize.SCREEN_HEIGHT/8

        // wave curvature (default: 1.5)
        waterView?.waveCurvature = 1.2

        waterView!.addOverView(avatarView)
        waterView!.backgroundColor = UIColor(red: 248/255, green: 64/255, blue: 87/255, alpha: 1)

        // Add WaveView
        self.view.addSubview(waterView!)

        // Start wave
        waterView!.start()
    }
    func addwaveanimation(imageName:String,innerV_:UIView,frame_:CGRect,tag:Int)  {
        let avatarFrame = CGRect(x: 0, y: 0, width: frame_.width, height: frame_.height)
              let avatarView = UIImageView(frame: avatarFrame)
//              avatarView.layer.cornerRadius = avatarView.bounds.height/2
//              avatarView.layer.masksToBounds = true
//              avatarView.layer.borderColor  = UIColor.white.cgColor
//              avatarView.layer.borderWidth = 3
              avatarView.layer.contents = UIImage(named: imageName)?.cgImage

        let frame = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: ScreenSize.SCREEN_HEIGHT/2)
              waterView = YXWaveView(frame: frame, color: UIColor.white)
              waterView!.addOverView(avatarView)
              waterView!.backgroundColor = UIColor(red: 8/255, green: 53/255, blue: 112/255, alpha: 1)

              // Add WaveView
              self.view.addSubview(waterView!)

              // Start wave
              waterView!.start()
    }
    
    
    func addBackgrondAnimationuToView(ContainerV:UIView,frame:CGRect)  {
        self.addAnimationView(withname: animationViewFile.background.rawValue, frame: frame, onview: ContainerV, animationV: animationView_background!)
                                      animationView_background?.play()
    }
    
    func addWaterVToView(containerV:UIView,frame_:CGRect,overview_:UIView)  {
         let frame = frame_
           waterView = YXWaveView(frame: frame, color: UIColor.white)
           waterView!.addOverView(overview_)
        waterView!.backgroundColor = UIColor.green
        // real wave color
        waterView?.realWaveColor = UIColor.blue
        
        // mask wave color
        waterView?.maskWaveColor = UIColor.black

        // wave speed (default: 0.6)
        waterView?.waveSpeed = 0.5

        // wave height (default: 5)
        waterView?.waveHeight = 6

        // wave curvature (default: 1.5)
        waterView?.waveCurvature = 1.2
           // Add WaveView
           containerV.addSubview(waterView!)

           // Start wave
           waterView!.start()
    }
    func ShowAlertViewWithAction(message:String,buttonCount:Int,ONVC:UIViewController,AlertTag:Int,isSuccess:Bool)  {
        
        let storyboard = UIStoryboard(name: "AlertViewController", bundle: nil)
       let alertvc = storyboard.instantiateViewController(withIdentifier: "AlertViewController") as? AlertViewController
        alertvc?.delegate = self
        alertvc?.msgString = message
        alertvc?.isSuccess = isSuccess
        alertvc?.alerttag = AlertTag
        alertvc?.alertTypeEnum = AlertTypeEnum(rawValue: buttonCount)
        ONVC.addChild(alertvc!)
        ONVC.view.addSubview((alertvc?.view)!)
        ONVC.tabBarController?.tabBar.isUserInteractionEnabled = false
        ONVC.navigationController?.navigationBar.isUserInteractionEnabled = false

        alertvc?.didMove(toParent: ONVC)
        
    }
    func addGravity(toImageV:UIImageView) {
//               var originalRect_: CGRect!
//
//               originalRect_ = toImageV.frame
//                      toImageV.frame = originalRect_
                      dynamicAnimator = UIDynamicAnimator(referenceView: view)
                     
               gravityBehavior  = UIGravityBehavior(items: [toImageV])
               dynamicAnimator.addBehavior(gravityBehavior)
               gravityBehavior.gravityDirection = CGVector(dx: 0, dy: 1)

           }
           
           func addBounce(toImageV:UIImageView) {
               collisionBehavior = UICollisionBehavior(items: [toImageV])
               collisionBehavior.translatesReferenceBoundsIntoBoundary = true
               
               
               dynamicAnimator.addBehavior(collisionBehavior)
               
               bouncingBehavior = UIDynamicItemBehavior(items: [toImageV])
               bouncingBehavior.elasticity = 0.70
               dynamicAnimator.addBehavior(bouncingBehavior)
           }
    func addAnimationView(withname:String,frame:CGRect,onview:UIView,animationV:AnimationView)  {
//         animationV = .init(name: withname)
             
        animationV.frame = frame
                //CGRect(x: 0, y: 0, width: self.view.frame.width, height: 200)
          
             animationV.contentMode = .scaleAspectFit
             
             animationV.loopMode = .loop
           
             animationV.animationSpeed = 0.5
            onview.addSubview(animationV)

        
        
        
//         animationView_backgroundkids = .init(name: "kids-logo")
//
//        animationView_backgroundkids!.frame = CGRect(x: 0, y: self.view.frame.height-250, width: self.view.frame.width, height: 200)
//
//         animationView_backgroundkids!.contentMode = .scaleAspectFit
//
//         animationView_backgroundkids!.loopMode = .loop
//
//         animationView_backgroundkids!.animationSpeed = 0.5
//        self.view.addSubview(animationView_backgroundkids!)
    
    }
    
    func customizeGivenButtonwithText(ginvenbutton:UIButton,text:String,fontsize:CGFloat,backgrouncolor:UIColor,textcolor:UIColor)  {
        ginvenbutton.titleLabel?.font = Utility.fontWithSize(size: (fontsize * Utility.getScreenScaleFactor()))
        ginvenbutton.layer.cornerRadius = 10
        ginvenbutton.layer.borderWidth = 2
        ginvenbutton.layer.borderColor = UIColor.black.cgColor
        ginvenbutton.backgroundColor = backgrouncolor
        ginvenbutton.titleLabel?.textColor = textcolor
        ginvenbutton.titleLabel?.textAlignment = .center
        ginvenbutton.setTitle(text, for: .normal)
             
    }
    
    func createParticlesOnView(containerV:UIView) {

           particleEmitter.emitterPosition = CGPoint(x: view.center.x, y: -96)
           particleEmitter.emitterShape = .line
           particleEmitter.emitterSize = CGSize(width: view.frame.size.width, height: 1)

           let red = makeEmitterCell(color: UIColor.red)
           let green = makeEmitterCell(color: UIColor.green)
           let blue = makeEmitterCell(color: UIColor.blue)

           particleEmitter.emitterCells = [red, green, blue]
        containerV.layer.insertSublayer(particleEmitter, at: 0)

//           containerV.layer.addSublayer(particleEmitter)
       }
    
    func makeEmitterCell(color: UIColor) -> CAEmitterCell {
        let cell = CAEmitterCell()
        cell.birthRate = 3
        cell.lifetime = 7.0
        cell.lifetimeRange = 0
        cell.color = color.cgColor
        cell.velocity = 200
        cell.velocityRange = 50
        cell.emissionLongitude = CGFloat.pi
        cell.emissionRange = CGFloat.pi / 4
        cell.spin = 2
        cell.spinRange = 3
        cell.scaleRange = 0.5
        cell.scaleSpeed = -0.05

        cell.contents = UIImage(named: "drop")?.cgImage
        return cell
    }
    
    func stopSpeech()  {
           speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    func speakgivenSpeech(speech:String)  {
             // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
             let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: speech)
             //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
             speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 4.0
             // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
        speechUtterance.rate = 0.35
        speechUtterance.pitchMultiplier = 2.0
                 speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
             // Line 5. Pass in the urrerance to the synthesize
        speechSynthesizer.speak(speechUtterance)

    }
    
    func setGradient(toView:UIView,withColor:[CGColor]) {
           let gradientLayer = CAGradientLayer()
           gradientLayer.frame = toView.bounds
           gradientLayer.colors = [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor]
           gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
           toView.layer.insertSublayer(gradientLayer, at: 0)
       }
    
    func setupfontAwesomeIconToButton(button:UIButton,iconname:FontAwesome,fontsize:CGFloat,color:UIColor)  {
             button.titleLabel?.font = UIFont.fontAwesome(ofSize: fontsize, style: .solid)
               button.setTitle(String.fontAwesomeIcon(name: iconname), for: .normal)
               button.setTitleColor(color, for: .normal)
           }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonClickedPlayer = self.createPlayer(from: audiofilename.buttonclicked.rawValue)

        // Do any additional setup after loading the view.
    }
    
    func moveToListVC(selectedItemName:String)  {
               let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController
                       vc?.selectedItems = selectedItemName
                                self.navigationController?.pushViewController(vc!, animated: true)
                   

            }
      func moveToCircularVC(selectedItemName:String)  {
              let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CircularViewController") as? CircularViewController
           vc?.selectedItems = selectedItemName
              self.navigationController?.pushViewController(vc!, animated: true)

          }
          func moveToStoryRhymesVC(selectedItemName:String)  {
                 let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RhymesStoriesListViewController") as? RhymesStoriesListViewController
              vc?.selectedItems = selectedItemName

                 self.navigationController?.pushViewController(vc!, animated: true)

             }
      func moveToSlideShowVC(selectedIndex:Int,button:UIButton,selectedItems:String)  {
                guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SlideShowViewController") as? SlideShowViewController else {
                    return
                }
    //            vc.selectedIndex = selectedIndex
    //            vc.selectedItems = selectedItems
                self.transition = JTMaterialTransition(animatedView: (button))
                                  
                                                      vc.modalPresentationStyle = .custom
                                                                vc.transitioningDelegate = self.transition
                                                                       
                                                                self.present(vc, animated: true, completion: nil)
        //

               }
        func moveTodrawingVC(selectedIndex:Int,button:UIButton,selectedItems:String)  {
            guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DrawingViewController") as? DrawingViewController else {
                return
            }
//            vc.selectedIndex = selectedIndex
//            vc.selectedItems = selectedItems
            self.transition = JTMaterialTransition(animatedView: (button))
                              
                                                  vc.modalPresentationStyle = .custom
                                                            vc.transitioningDelegate = self.transition
                                                                   
                                                            self.present(vc, animated: true, completion: nil)
    //

           }
    func moveToStoryRhymes(selectedIndex:Int,button:UIButton,selectedItems:String)  {
        guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "StoryRhymesViewController") as? StoryRhymesViewController else {
            return
        }
        vc.selectedIndex = selectedIndex
        vc.selectedItems = selectedItems
        self.transition = JTMaterialTransition(animatedView: (button))
                          
                                              vc.modalPresentationStyle = .custom
                                                        vc.transitioningDelegate = self.transition
                                                               
                                                        self.present(vc, animated: true, completion: nil)
//

       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
