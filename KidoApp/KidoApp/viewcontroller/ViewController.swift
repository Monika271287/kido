//
//  ViewController.swift
//  KidoApp
//
//  Created by monika kumari on 13/05/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import  JTMaterialTransition
import SpriteKit
import CoreMotion
import Lottie


class ViewController: BaseViewController,UIPopoverPresentationControllerDelegate,SoundContentControllerDelegate, LEARNLISTControllerDelegate {
   
    var animationView_hello: AnimationView? = .init(name: animationViewFile.welcome.rawValue)
    var animationView_kids: AnimationView? = .init(name: animationViewFile.kidslogo.rawValue)
    let manager = CMMotionManager()
//    @IBOutlet weak var sceneView: SKView!

    @IBOutlet weak var innerV: UIView!
//    let emitterScene = ParticleScene()

//   let emitterNode = SKEmitterNode(fileNamed: "Magicparticle.sks")!
    
    @IBOutlet var learnButton: UIButton!
    @IBOutlet var drawButton: UIButton!
    @IBOutlet var playButton: UIButton!

   override func viewDidDisappear(_ animated: Bool) {
    NotificationCenter.default.removeObserver(self, name: Notification.Name.NSExtensionHostWillEnterForeground, object: nil)
    }
 @objc  override func applicationWillEnterForeground(_ notification: NSNotification) {
    self.animationView_hello!.play()
    self.animationView_kids!.play()
    
    
    }
    override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
//        NotificationCenter.default.addObserver(forName: NSNotification.Name.NSExtensionHostWillEnterForeground, object: nil, queue: nil) { notification in
//            self.animationView_hello!.play()
//                   self.animationView_kids!.play()
//
//        }
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(applicationWillEnterForeground(_:)),
        name: UIApplication.willEnterForegroundNotification,
        object: nil)

      
      
    }
    override func viewDidLoad() {
          super.viewDidLoad()
//        self.setupfontAwesomeIconToButton(button: self.playButton, iconname: .gamepad, fontsize: (20 * Utility.getScreenScaleFactor()) , color: UIColor.white)
//        self.setupfontAwesomeIconToButton(button: self.drawButton, iconname: .paintBrush, fontsize: (20 * Utility.getScreenScaleFactor()) , color: UIColor.white)
//        self.setupfontAwesomeIconToButton(button: self.learnButton, iconname: .book, fontsize: (20 * Utility.getScreenScaleFactor()) , color: UIColor.white)


        dLog(message: "")
       // self.addRain()

        self.addAnimationView(withname: animationViewFile.welcome.rawValue, frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height/2), onview: self.view, animationV: animationView_hello!)
        self.addAnimationView(withname: animationViewFile.kidslogo.rawValue, frame: CGRect(x: 0, y: self.view.frame.height-250, width: self.view.frame.width, height: 200), onview: self.view, animationV: animationView_kids!)


        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])

        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden=true
        self.animationView_hello!.play()
        self.animationView_kids!.play()
        
    }
    @IBAction func DrawButtonClicked(_ sender: UIButton) {
         sender.flash()
    self.buttonClickedPlayer?.play()
        self.transition = JTMaterialTransition(animatedView: self.drawButton)
                      let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LearnListViewController") as? LearnListViewController
                     // self.navigationController?.pushViewController(vc!, animated: true)'
                      vc?.delegate = self
                      vc?.type = learnListType.draw
                      vc?.modalPresentationStyle = .custom
                      vc?.transitioningDelegate = self.transition
               self.navigationController?.pushViewController(vc!, animated: true)
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SlideShowViewController") as? SlideShowViewController
//        //self.navigationController?.pushViewController(vc!, animated: true)
//        vc?.modalPresentationStyle = .custom
//              vc?.transitioningDelegate = self.transition
//
//              self.present(vc!, animated: true, completion: nil)
    }
    @IBAction func playButtonClicked(_ sender: UIButton) {
        sender.flash()

       self.buttonClickedPlayer?.play()
               self.transition = JTMaterialTransition(animatedView: self.playButton)
               let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LearnListViewController") as? LearnListViewController
              // self.navigationController?.pushViewController(vc!, animated: true)'
               vc?.delegate = self
               vc?.type = learnListType.play
               vc?.modalPresentationStyle = .custom
               vc?.transitioningDelegate = self.transition
        self.navigationController?.pushViewController(vc!, animated: true)
              // self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func learnButtonClicked(_ sender: UIButton) {
        sender.flash()
        self.buttonClickedPlayer?.play()
        self.transition = JTMaterialTransition(animatedView: self.learnButton)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LearnListViewController") as? LearnListViewController
        vc?.delegate = self
        vc?.type = learnListType.learn
      self.navigationController?.pushViewController(vc!, animated: true)
    
    }
    
    //UIPopoverPresentationControllerDelegate inherits from UIAdaptivePresentationControllerDelegate, we will use this method to define the presentation style for popover presentation controller
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    return .none
    }
     
    //UIPopoverPresentationControllerDelegate
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
     
    }
    func moveToGameVC(selectedItemName:String,controllerC:UIViewController)  {
        guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else { return  }
        self.transition = JTMaterialTransition(animatedView: self.learnButton)
//             vc?.selectedItems = selectedItemName
        vc.delegate = controllerC as! GameViewControllerDelegate
        self.present(vc, animated: true, completion: nil)
        
       

    }
  
    func popoverPresentationControllerShouldDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) -> Bool {
    return true
    }
    func popoverContentFORLearnlist(controller: UIViewController, didselectItem name: String,type:learnListType) {
        if type == learnListType.learn{
        switch name {
        case learnListItems.story.rawValue:
            self.moveToStoryRhymesVC(selectedItemName: name)
            
                case learnListItems.rhymes.rawValue:
                self.moveToStoryRhymesVC(selectedItemName: name)
            
        case learnListItems.color.rawValue:
                self.moveToCircularVC(selectedItemName: name)
            
        case learnListItems.shapes.rawValue:
            self.moveToCircularVC(selectedItemName: name)
            
            
            case learnListItems.animals.rawValue:
                       self.moveToListVC(selectedItemName: name)
                       
                           case learnListItems.birds.rawValue:
                           self.moveToListVC(selectedItemName: name)
                       
                   case learnListItems.numbers.rawValue:
                           self.moveToListVC(selectedItemName: name)
                       
                   case learnListItems.alphabets.rawValue:
                       self.moveToListVC(selectedItemName: name)

        default:
                            self.moveToListVC(selectedItemName: name)

        }
        }
        else{
            self.moveToGameVC(selectedItemName: name,controllerC:controller)

        }
      }
    func popoverContent(controller: SoundContentController, didselectItem name: String) {
       // showButton.setTitle(name, for: .normal)
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController
        vc?.selectedItems = name
                 self.navigationController?.pushViewController(vc!, animated: true)
    }
    func movetoLearnlistFromGame(game: Int) {
                     let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LearnListViewController") as? LearnListViewController
                    //             vc?.selectedItems = selectedItemName
        
                                            self.navigationController?.pushViewController(vc!, animated: true)

    }
    
    
}

