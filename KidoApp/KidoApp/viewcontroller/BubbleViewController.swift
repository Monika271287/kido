//
//  BubbleViewController.swift
//  KidoApp
//
//  Created by monika kumari on 01/08/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import SpriteKit
import Magnetic
import UIKit
extension UIControl {

    func flashUIControl() {
    let flash = CABasicAnimation(keyPath: "opacity")
    flash.duration = 0.3
    flash.fromValue = 1
    flash.toValue = 0.1
    flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    flash.autoreverses = true
    flash.repeatCount = 2
    layer.add(flash, forKey: nil)
    }
}

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255, alpha: 1)
    }
    
    static var red: UIColor {
        return UIColor(red: 255, green: 59, blue: 48)
    }
    
    static var orange: UIColor {
        return UIColor(red: 255, green: 149, blue: 0)
    }
    
    static var yellow: UIColor {
        return UIColor(red: 255, green: 204, blue: 0)
    }
    
    static var green: UIColor {
        return UIColor(red: 76, green: 217, blue: 100)
    }
    
    static var tealBlue: UIColor {
        return UIColor(red: 90, green: 200, blue: 250)
    }
    
    static var blue: UIColor {
        return UIColor(red: 0, green: 122, blue: 255)
    }
    
    static var purple: UIColor {
        return UIColor(red: 88, green: 86, blue: 214)
    }
    
    static var pink: UIColor {
        return UIColor(red: 255, green: 45, blue: 85)
    }
    
    static let colors: [UIColor] = [.red, .orange, .yellow, .green, .tealBlue, .blue, .purple, .pink]
    
}
extension Array {
    
    func randomItem() -> Element {
        let index = Int(arc4random_uniform(UInt32(self.count)))
        return self[index]
    }
    
}

extension CGPoint {
    
    func distance(from point: CGPoint) -> CGFloat {
        return hypot(point.x - x, point.y - y)
    }
    
}
extension UIImage {
    
    static let names: [String] = ["apple","ball","cat","dog","elephant","fish","giraffe","horse","ice-cream","jug","kite","lion","mango","nest","octopus","parrot","queen","rabbit","sun","tortoise","umbrella","vase","watch","x-mas tree","yak","zebra"]
    
}

class BubbleViewController: BaseViewController {

    var arrayOfItem :[String] = [String]()
    @IBOutlet var addButton: UIButton!

    @IBOutlet var resetButton: UIButton!
    
    var item:String = String()
    var index:Int = Int()
  var selecteditem:String = String()

    
    @IBOutlet weak var magneticView: MagneticView! {
        didSet {
            magnetic.magneticDelegate = self
            magnetic.removeNodeOnLongPress = true
            magnetic.isDragging = true
            #if DEBUG
            magneticView.showsFPS = true
            magneticView.showsDrawCount = false
            magneticView.showsQuadCount = false
            magneticView.showsPhysics = false
            magneticView.showsFPS = false
            #endif
        }
    }
    
    var magnetic: Magnetic {
        return magneticView.magnetic
    }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        self.view.backgroundColor = UIColor.black
        debugPrint("item=====\(item),index====\(index)")
        
              switch selecteditem {
        case learnListItems.animals.rawValue:
                arrayOfItem = ["cat", "rat", "pig", "cow", "lion", "rooster", "dog", "monkey"]
                
          
            case learnListItems.birds.rawValue:
                    arrayOfItem = ["eagle", "parrot", "duck", "flamingo", "kingfisher", "crow", "pigeon", "peacock"]
            
            case learnListItems.instruments.rawValue:
                     
                arrayOfItem = ["DRUM", "SAXOPHONE", "VIALIN", "FLUTE", "GUITAR"]


            case learnListItems.alphabets.rawValue:
             
                arrayOfItem = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

            case learnListItems.numbers.rawValue:
                                        
                arrayOfItem = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

        default:
        print("")
        }
        
        
               self.setupfontAwesomeIconToButton(button: resetButton, iconname: .undo, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)
        self.setupfontAwesomeIconToButton(button: addButton, iconname: .plus, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)


    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let arraycount:Int = arrayOfItem.count
        for _ in 0..<arraycount {
            add(nil)
        }
    }
    
    @IBAction func add(_ sender: UIControl?) {
        sender?.flashUIControl()
        self.buttonClickedPlayer?.play()
           self.buttonClickedPlayer?.play()
        let name = arrayOfItem.randomItem()
        let color = UIColor.colors.randomItem()
        let node = Node(text: name.capitalized, image: UIImage(named: name), color: color, radius: 40)
        node.scaleToFitContent = true
        node.selectedColor = UIColor.colors.randomItem()
        magnetic.addChild(node)
        
        // Image Node: image displayed by default
        // let node = ImageNode(text: name.capitalized, image: UIImage(named: name), color: color, radius: 40)
        // magnetic.addChild(node)
    }
    
    @IBAction func reset(_ sender: UIControl?) {
        sender?.flashUIControl()
        self.buttonClickedPlayer?.play()
        magneticView.magnetic.reset()
    }
    
}

// MARK: - MagneticDelegate
extension BubbleViewController: MagneticDelegate {
    
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        print("didSelect -> \(node)")
        let animal:String = node.text?.lowercased() ?? ""
        self.speakgivenSpeech(speech: animal)

        
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        print("didDeselect -> \(node)")
    }
    
    func magnetic(_ magnetic: Magnetic, didRemove node: Node) {
        print("didRemove -> \(node)")
    }
    
}

// MARK: - ImageNode
class ImageNode: Node {
    override var image: UIImage? {
        didSet {
            texture = image.map { SKTexture(image: $0) }
        }
    }
    override func selectedAnimation() {}
    override func deselectedAnimation() {}
}

