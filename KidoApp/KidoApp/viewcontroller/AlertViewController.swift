//
//  AlertViewController.swift
//  RateSurveyApp
//
//  Created by Apple on 6/28/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Foundation
@objc protocol AlertViewControllerDelegate  {
    func YESAction(_ sender: UIButton)
    func NOAction(_ sender: UIButton)
    
}
class AlertViewController: UIViewController {

    @IBOutlet var flexiWidthConstraint: NSLayoutConstraint!
    @IBOutlet var btnNo: UIButton!
    @IBOutlet var btnYes: UIButton!
    @IBOutlet var lblAlertMsg: UILabel!
    var delegate:AlertViewControllerDelegate?
    var alertTypeEnum:AlertTypeEnum?
    var msgString:String?
    var titleString:String?

     var isSuccess:Bool?
    var alerttag:Int?
    func cancelClicked() {
        self.parent?.tabBarController?.tabBar.isUserInteractionEnabled = true
        self.parent?.navigationController?.navigationBar.isUserInteractionEnabled = true
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    @IBAction func backgroundClicked(_ sender: UIButton) {
       // cancelClicked()
    }
    func setFontAndColor()  {
        lblAlertMsg.textColor = UIColor.white
        lblAlertMsg.font = Utility.fontWithSize(size: (17 * Utility.getScreenScaleFactor()))
        lblAlertMsg.numberOfLines = 0
        let arrbuttons:[UIButton]   = [btnYes,btnNo]
        arrbuttons.forEach({ button in
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = Utility.fontWithSize(size: (16 * Utility.getScreenScaleFactor()))
            button.backgroundColor = Utility.getOrangeColor()

        })
        
    }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        dLog(message: "Log this!")
        setFontAndColor()
    

        if alertTypeEnum == AlertTypeEnum.onebutton{
            lblAlertMsg.text = msgString

            flexiWidthConstraint = flexiWidthConstraint.setMultiplier(multiplier: 0.5)
            btnNo.isHidden = true
            btnYes.isHidden = false
            btnYes.setTitle("OK", for: .normal)
            
        }
        else{
            lblAlertMsg.text = msgString
            
            flexiWidthConstraint = flexiWidthConstraint.setMultiplier(multiplier: 1)
            btnNo.isHidden = false
            btnYes.isHidden = false
//            btnYes.setTitle("OK", for: .normal)
            
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func btnNoClicked(_ sender: UIButton) {
        sender.tag = alerttag!
        delegate?.NOAction(sender)
        self.cancelClicked()
    }
    @IBAction func btnYesClicked(_ sender: UIButton) {
        sender.tag = alerttag!
        self.cancelClicked()

        delegate?.YESAction(sender)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
