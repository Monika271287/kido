//
//  ListViewController.swift
//  DrawingBook
//
//  Created by monika kumari on 13/05/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
class AnimalListCollectionViewCell: UICollectionViewCell {

    @IBOutlet var heightConstant: NSLayoutConstraint!
    @IBOutlet weak var animalNameLabel: UILabel!
    @IBOutlet weak var animalImgV: UIImageView!
    

}
class ListViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var alphabetsArray = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    var numbersArray = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]

    var birdsArray = ["EAGLE", "PARROT", "DUCK", "FLAMINGO", "KINGFISHER", "CROW", "PIGEON", "PEACOCK"]

    var instrumentsArray = ["DRUM", "SAXOPHONE", "VIALIN", "FLUTE", "GUITAR"]

    var animals = ["CAT", "RAT", "PIG", "COW", "LION", "ROOSTER", "DOG", "MONKEY"]
    var animals_image = ["cat", "rat", "pig", "cow", "lion", "rooster", "dog", "monkey"]
    var birds_image = ["eagle", "parrot", "duck", "flamingo", "kingfisher", "crow", "pigeon", "peacock"]

    var instruments_image = ["DRUM", "SAXOPHONE", "VIALIN", "FLUTE", "GUITAR"]

    @IBOutlet weak var collectionV: UICollectionView!
    var selectedIndex = Int()
    var selectedItems = String()


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden=false
        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])
        self.collectionV.backgroundColor = UIColor.clear
      
        
                   self.title = selectedItems


        // Do any additional setup after loading the view.
    }
    
    @IBAction func AnimalButtonClicked(_ sender: UIButton) {
          

           let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PlayViewController") as? PlayViewController
        self.buttonClickedPlayer?.play()

           self.navigationController?.pushViewController(vc!, animated: true)
       }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
      // MARK: - UICollectionViewDataSource protocol
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1     //return number of sections in collection view
    }

        // tell the collection view how many cells to make
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            switch selectedItems {
            case "ANIMALS":
                            return self.animals.count
                case "BIRDS":
                                           return self.birdsArray.count
                case "INSTRUMENTS":
                                           return self.instrumentsArray.count
                case "ALPHABETS":
                                           return self.alphabetsArray.count
                case "NUMBERS":
                                           return self.numbersArray.count

            default:
                            return 0

            }
        }

        // make a cell for each cell index path
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            // get a reference to our storyboard cell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! AnimalListCollectionViewCell

            // Use the outlet in our custom class to get a reference to the UILabel in the cell
            if UIDevice.current.userInterfaceIdiom == .phone{
                
            }
            else{
                
            }
            
        
            switch self.selectedItems {
                
            case learnListItems.animals.rawValue:
            cell.animalImgV.isHidden = false
            cell.animalNameLabel.text = self.animals[indexPath.item]
            cell.animalNameLabel.font = Utility.fontWithSize(size: (20 * Utility.getScreenScaleFactor()))
            cell.animalImgV.image = UIImage(named: self.animals_image[indexPath.item])
            return cell
                
            case learnListItems.birds.rawValue:
                cell.animalImgV.isHidden = false
                     cell.animalNameLabel.text = self.birdsArray[indexPath.item]
                     cell.animalNameLabel.font = Utility.fontWithSize(size: (20 * Utility.getScreenScaleFactor()))
            cell.animalImgV.image = UIImage(named: self.birds_image[indexPath.item])
            return cell

                case learnListItems.instruments.rawValue:
                    cell.animalImgV.isHidden = false
                                        cell.animalNameLabel.text = self.instrumentsArray[indexPath.item]
                                        cell.animalNameLabel.font = Utility.fontWithSize(size: (20 * Utility.getScreenScaleFactor()))
                                               cell.animalImgV.image = UIImage(named: self.instruments_image[indexPath.item])
                               return cell

                
                case learnListItems.alphabets.rawValue:
                    cell.animalImgV.isHidden = true
                    cell.animalNameLabel.backgroundColor = UIColor.clear
                    cell.animalNameLabel.font = Utility.fontWithSize(size: (70 * Utility.getScreenScaleFactor()))
                    let newMultiplier:CGFloat = 1.0
                    cell.heightConstant = cell.heightConstant.setMultiplier(multiplier: newMultiplier)
                    cell.animalNameLabel.text = alphabetsArray[indexPath.row]

                    
                                            
                               return cell

                case learnListItems.numbers.rawValue:
                    cell.animalImgV.isHidden = true
                    cell.animalNameLabel.backgroundColor = UIColor.clear
                    cell.animalNameLabel.font = Utility.fontWithSize(size: (70 * Utility.getScreenScaleFactor()))
                    let newMultiplier:CGFloat = 1.0
                    cell.heightConstant = cell.heightConstant.setMultiplier(multiplier: newMultiplier)
                    cell.animalNameLabel.text = numbersArray[indexPath.row]

                                               
                               return cell

                

            default:
                                               return cell

            }
          
            cell.backgroundColor = UIColor.clear // make cell more visible in our example project

            return cell
        }

        // MARK: - UICollectionViewDelegate protocol

        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            // handle tap events
            print("You selected cell #\(indexPath.item)!")
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PlayViewController") as? PlayViewController
            vc?.selectedIndex = indexPath.row
            vc?.selecteditem = self.selectedItems

            switch selectedItems {
                       case "ANIMALS":
                        
                        vc?.selectedArray = self.animals
                 vc?.selectedArrayImage = self.animals_image
                           case "BIRDS":
                                                    vc?.selectedArray = self.birdsArray
                 vc?.selectedArrayImage = self.birds_image
                
                           case "INSTRUMENTS":
                                                      vc?.selectedArray = self.instrumentsArray
                 vc?.selectedArrayImage = self.instruments_image
                           case "ALPHABETS":
                                                      vc?.selectedArray = self.alphabetsArray
                 vc?.selectedArrayImage = []
                           case "NUMBERS":
                                                     vc?.selectedArray = self.numbersArray
                 vc?.selectedArrayImage = []

                       default:
                                        vc?.selectedArray = []
                 vc?.selectedArrayImage = []

                       }

            let cell = collectionView.cellForItem(at: indexPath) as! AnimalListCollectionViewCell
            vc?.selecteditemName = cell.animalNameLabel.text!
            if (selectedItems == learnListItems.animals.rawValue) || (selectedItems == learnListItems.birds.rawValue) || (selectedItems == learnListItems.instruments.rawValue){
                vc?.selecteditemImage = cell.animalImgV.image!
            }
           
            self.buttonClickedPlayer?.play()



                   self.navigationController?.pushViewController(vc!, animated: true)
               
            
        }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let screenWidth = UIScreen.main.bounds.width
           let scaleFactor = (screenWidth/2)
           
           return CGSize(width: scaleFactor, height: 240)
       }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
    

}
