//
//  CircularViewController.swift
//  KidoApp
//
//  Created by monika kumari on 25/05/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit

extension UIColor {
    var name: String? {
        switch self {
            case UIColor.lightGray: return "lightgray"

        case UIColor.red: return "red"
        case UIColor.orange: return "orange"
        case UIColor.yellow: return "yellow"
        case UIColor.green: return "green"
        case UIColor.blue: return "blue"
        case UIColor.purple: return "purple"
        case UIColor.brown: return "brown"
        case UIColor.magenta: return "magenta"
        case UIColor.cyan: return "cyan"
        case UIColor.systemPink: return "pink"
        case UIColor.black: return "black"
        case UIColor.white: return "white"
        default: return nil
        }
    }
}
class CircularViewController: BaseViewController {
  
    
  var rotatingViews = [RotatingView]()
  let numberOfViews = 4
    var startingIndex = 0
    var colorIndex =  0

  var circle = Circle(center: CGPoint(x: 200, y: 400), radius: 100)
  var prevLocation = CGPoint.zero
    var selectedIndex = Int()
       var selectedItems = String()
    var colorArray:[UIColor] = [UIColor()]
    var newArray:[UIColor] = [UIColor]()
    
    
    func setUpNavigationBar()  {
         self.navigationController?.navigationBar.isHidden=false
           let image = UIImage.fontAwesomeIcon(name: .redo, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
           let nextBtn = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.nextClicked(_:)))
           self.navigationItem.rightBarButtonItem  = nextBtn
           
    }
  override func viewDidLoad() {
      super.viewDidLoad()
        dLog(message: "")
    self.setUpNavigationBar()
   
    
//    self.customizeGivenButtonwithText(ginvenbutton: nextButton, text: "NEXT", fontsize: 20.0, backgrouncolor: UIColor.orange, textcolor: UIColor.white)
    colorArray = [UIColor.lightGray,UIColor.red,
                  UIColor.orange,
                  UIColor.yellow,
                  UIColor.green,
                  UIColor.blue,
                  UIColor.purple,
                  UIColor.brown,
                  UIColor.magenta,
                  UIColor.cyan,
                  UIColor.systemPink,
                  UIColor.black,
                  UIColor.white,
                  UIColor.gray]
    

    for i in 0...numberOfViews {
        
      let angleBetweenViews = (2 * Double.pi) / Double(numberOfViews)
        let arraySlice = colorArray[startingIndex...startingIndex+4] // also works
        newArray = Array(arraySlice)// prints: ["A", "B", "C", "D", "E"]
        let viewOnCircle = RotatingView(circle: circle, angle: CGFloat(Double(i) * angleBetweenViews), index: colorIndex,colorArray:newArray)
      rotatingViews.append(viewOnCircle)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.TappedMethod(_:)))
        viewOnCircle.tag = i
               viewOnCircle.isUserInteractionEnabled = true
             viewOnCircle.addGestureRecognizer(tapGestureRecognizer)
      view.addSubview(viewOnCircle)
        colorIndex = colorIndex + 1
    }

    let panGesture = UIPanGestureRecognizer(target: self, action: #selector(didPan(panGesture:)))
    view.addGestureRecognizer(panGesture)
  }
    
    @objc func nextClicked(_ sender: UIBarButtonItem) {
        print(startingIndex)
        if (startingIndex + 4) < (colorArray.count)-4{
        startingIndex = startingIndex + 4
        }
        else{
            startingIndex = 0
        }
                   //self.dismiss(animated: true, completion: nil)removeFromSuperview(self.view as! RotatingView)
      
        for subview in view.subviews{
            
            if !(subview is UIButton) {
            subview.removeFromSuperview()
            }
        }
        for i in 0...numberOfViews {
             let angleBetweenViews = (2 * Double.pi) / Double(numberOfViews)
               let arraySlice = colorArray[startingIndex...(startingIndex+4)] // also works
              newArray = Array(arraySlice)// prints: ["A", "B", "C", "D", "E"]
               let viewOnCircle = RotatingView(circle: circle, angle: CGFloat(Double(i) * angleBetweenViews), index: i,colorArray:newArray)
             rotatingViews.append(viewOnCircle)
               let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.TappedMethod(_:)))
                viewOnCircle.tag = i
           
                      viewOnCircle.isUserInteractionEnabled = true
                    viewOnCircle.addGestureRecognizer(tapGestureRecognizer)
             view.addSubview(viewOnCircle)
           }

        

       }
    @IBAction func backClicked(_ sender: UIButton) {
                self.dismiss(animated: true, completion: nil)

    }
    @objc func TappedMethod(_ sender:AnyObject){
            print("you tap image number: \(sender.view.tag)")
        if sender.view.tag < newArray.count{
        self.view.backgroundColor = newArray[sender.view.tag ]
        let selectedColor:UIColor =  newArray[sender.view.tag] as! UIColor
        let str:String =  selectedColor.name ?? "no color"
        print(str)
        self.speakgivenSpeech(speech: str)
        }

       }

  @objc func didPan(panGesture: UIPanGestureRecognizer){
    switch panGesture.state {
    case .began:
      prevLocation = panGesture.location(in: view)
    case .changed, .ended:
      let nextLocation = panGesture.location(in: view)
      let angle = circle.angleBetween(firstPoint: prevLocation, secondPoint: nextLocation)

      rotatingViews.forEach({ $0.updatePosition(angle: angle)})
      prevLocation = nextLocation
    default: break
    }
  }
}


struct Circle {
  let center: CGPoint
  let radius: CGFloat

  func pointOnCircle(angle: CGFloat) -> CGPoint {
    let x = center.x + radius * cos(angle)
    let y = center.y + radius * sin(angle)

    return CGPoint(x: x, y: y)
  }

  func angleBetween(firstPoint: CGPoint, secondPoint: CGPoint) -> CGFloat {
    let firstAngle = atan2(firstPoint.y - center.y, firstPoint.x - center.x)
    let secondAnlge = atan2(secondPoint.y - center.y, secondPoint.x - center.x)
    let angleDiff = (firstAngle - secondAnlge) * -1

    return angleDiff
  }
}


class RotatingView: UIView {
  var currentAngle: CGFloat
    var currentIndex: Int
    var colors: [UIColor]
    var color = UIColor.black

  let circle: Circle

    init(circle: Circle, angle: CGFloat,index:Int,colorArray:[UIColor]) {
    self.currentAngle = angle
    self.circle = circle
    self.currentIndex = index
        self.colors = colorArray
        
       
    super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
    center = circle.pointOnCircle(angle: currentAngle)
            self.tag = currentIndex
            backgroundColor = self.colors[currentIndex]
                   self.backgroundColor = backgroundColor
                  

       
    self.layer.cornerRadius = self.frame.size.width/2
    self.clipsToBounds = true
    self.layer.borderColor = UIColor.black.cgColor
    self.layer.borderWidth = 5.0
        var imageView : UIImageView
        imageView  = UIImageView(frame:CGRect(x:0, y:0,width: self.frame.width, height:self.frame.height))
        imageView.image = UIImage(named:"bubble")
        self.addSubview(imageView)
        
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func updatePosition(angle: CGFloat) {
    currentAngle += angle
    center = circle.pointOnCircle(angle: currentAngle)
  }
}
