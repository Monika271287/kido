//
//  RainingViewController.swift
//  KidoApp
//
//  Created by monika kumari on 28/06/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//
import Foundation
import UIKit
import SpriteKit
import Lottie
extension CGRect {
    var center: CGPoint { return CGPoint(x: 0, y: 0) }
}
extension UILabel {
    func setTextWithTypeAnimation(typedText: String, characterDelay: TimeInterval = 5.0) {
        text = ""
        var writingTask: DispatchWorkItem?
        writingTask = DispatchWorkItem { [weak weakSelf = self] in
            for character in typedText {
                DispatchQueue.main.async {
                    weakSelf?.text!.append(character)
                }
                Thread.sleep(forTimeInterval: characterDelay/100)
            }
        }

        if let task = writingTask {
            let queue = DispatchQueue(label: "typespeed", qos: DispatchQoS.userInteractive)
            queue.asyncAfter(deadline: .now() + 0.05, execute: task)
        }
    }

}
class RainingViewController: BaseViewController {
    var charLayers = [CAShapeLayer]()
    var item:String = String()
    var index:Int = Int()
//    var animationView_background: AnimationView? = .init(name: "background")

    var alphabetsImages:[String] = [String]()

    @IBOutlet weak var innerV: UIView!
    @IBOutlet weak var outerV: UIView!
    @IBOutlet weak var centreOuterV: UIView!
    @IBOutlet weak var bottomOuterV: UIView!
    @IBOutlet weak var centreImageV: UIImageView!

    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var bottomTextLabel: UILabel!



    
    func customizeGivenLabelwithText(ginvenLbl:UILabel,text:String,fontsize:CGFloat)  {
          ginvenLbl.font = Utility.fontWithSize(size: (fontsize * Utility.getScreenScaleFactor()))
                     
                     ginvenLbl.text = text
              UIView.animate(withDuration: 1.0) {
                  ginvenLbl.transform = CGAffineTransform(scaleX: 2.0, y: 2.0)
              }
    }
    func createCircularShapeForView(givenV:UIView)  {
        givenV.layer.cornerRadius = 20
        givenV.clipsToBounds = true
        givenV.backgroundColor = UIColor.clear
        givenV.layer.borderColor = UIColor.black.cgColor
        givenV.layer.borderWidth = 2.0
    }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        self.addvolumeButtontoView(containerV: self.view)
        //self.addAnimationView()
        //self.createText()
//        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])

        self.addBackgrondAnimationuToView(ContainerV: self.view, frame: CGRect(x: 0 , y: 0, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT))
               self.view?.sendSubviewToBack(self.animationView_background!)
       alphabetsImages = ["apple","ball","cat","dog","elephant","fish","giraffe","horse","ice-cream","jug","kite","lion","mango","nest","octopus","parrot","queen","rabbit","sun","tortoise","umbrella","vase","watch","x-mas tree","yak","zebra"]
      
        
        self.createCircularShapeForView(givenV: outerV)
        self.createCircularShapeForView(givenV: bottomOuterV)

        self.customizeGivenLabelwithText(ginvenLbl: textLabel, text: "\(item)\(item.lowercased())", fontsize:(30 * Utility.getScreenScaleFactor()))
        self.customizeGivenLabelwithText(ginvenLbl: bottomTextLabel, text: self.alphabetsImages[index].uppercased(), fontsize:(15 * Utility.getScreenScaleFactor()))
        centreImageV.image = UIImage(named:self.alphabetsImages[index])
       // self.fontSizeAnimation(label: textLabel)
       //

        //self.createParticlesOnView(containerV: self.innerV)
        // Do any additional setup after loading the view.
    }
    @objc func volumeButtonPressed(sender:UIButton){
            sender.flash()
    self.buttonClickedPlayer?.play()
        let text1:String = item.uppercased()

        let text2:String = self.alphabetsImages[index].uppercased()

        let texttospell:String  = "'\(text1)'" + "for " + "'\(text2),'"
       // "'A' for apple, 'a','a','a'"
            //(bottomTextLabel.text)!.lowercased()
            //
        self.speakgivenSpeech(speech: texttospell)
       }
    func addvolumeButtontoView(containerV:UIView)  {
        let volumeButton:UIButton = UIButton(type: .custom)
        volumeButton.frame =  CGRect(x: self.view.frame.size.width - 70 , y: 80, width: 50, height: self.view.frame.height/6)
        volumeButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 130, style: .solid)
        volumeButton.setTitle(String.fontAwesomeIcon(name: .volumeDown), for: .normal)
        volumeButton.setTitleColor(UIColor.red, for: .normal)
        volumeButton.addTarget(self, action: #selector(self.volumeButtonPressed(sender:)), for: .touchUpInside)
        containerV.addSubview(volumeButton)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
