//
//  SlideShowViewController.swift
//  KidoApp
//
//  Created by monika kumari on 17/09/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import TLPhotoPicker
import Photos

import UIKit
import SwiftVideoGenerator
class SlideShowViewController: BaseViewController, TLPhotosPickerViewControllerDelegate {
    var selectedAssets = [TLPHAsset]()

    @IBOutlet var gallaryButton: UIButton!
    
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var saveButton: UIButton!
    var images:[UIImage] = [UIImage(named: "cat")!, UIImage(named: "rat")!, UIImage(named: "pig")!, UIImage(named: "cow")!, UIImage(named: "lion")!, UIImage(named: "rooster")!, UIImage(named: "dog")!, UIImage(named: "monkey")!]
    
    func setUpCustomButtons()  {
        self.setupfontAwesomeIconToButton(button: gallaryButton, iconname: .photoVideo, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)
        self.setupfontAwesomeIconToButton(button: saveButton, iconname: .save, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)
        self.setupfontAwesomeIconToButton(button: shareButton, iconname: .share, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)



    }
    
    override func viewDidLoad() {
          super.viewDidLoad()
        setUpCustomButtons()
          createVideo(audiofilename: "insy", extensionformat: "mp3", filename: "kido", backgroungcolor: .green)
          // Do any additional setup after loading the view.
      }
    
    
      
    // MARK: - Buttonclicked methods

    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    
    
    func createVideo(audiofilename:String,extensionformat:String,filename:String,backgroungcolor:UIColor)  {
       if let audioURL1 = Bundle.main.url(forResource: audiofilename, withExtension: extensionformat) {

          VideoGenerator.fileName = filename
          VideoGenerator.videoBackgroundColor = backgroungcolor
          VideoGenerator.videoImageWidthForMultipleVideoGeneration = 2000
        VideoGenerator.current.generate(withImages: images, andAudios: [audioURL1], andType: .singleAudioMultipleImage, {
            progress in
        }, outcome: {result  in
            print(result)
            switch result {
            case .success(let url):
                self.saveVideoToLibrary(videoURL: url)
            case .failure(let error):
                print(error.localizedDescription)
            }

        })
          
    }
    }
     
    @IBAction func gallaryButtonClicked(_ sender: Any) {
        let viewController = TLPhotosPickerViewController()
               viewController.delegate = self
               var configure = TLPhotosPickerConfigure()
               //configure.nibSet = (nibName: "CustomCell_Instagram", bundle: Bundle.main) // If you want use your custom cell..
               self.present(viewController, animated: true, completion: nil)
    }
    
       @IBAction func saveVideoToLibrary(videoURL: URL) {

        PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: videoURL)
        }) { saved, error in

            if let error = error {
                print("Error saving video to librayr: \(error.localizedDescription)")
            }
            if saved {
                print("Video save to library")

            }
        }
    }
    
    

  

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    //TLPhotosPickerViewControllerDelegate
    func shouldDismissPhotoPicker(withTLPHAssets: [TLPHAsset]) -> Bool {
        // use selected order, fullresolution image
        self.selectedAssets = withTLPHAssets
    return true
    }
    func dismissPhotoPicker(withPHAssets: [PHAsset]) {
        // if you want to used phasset.
    }
    func photoPickerDidCancel() {
        // cancel
    }
    func dismissComplete() {
        // picker viewcontroller dismiss completion
    }
//    func canSelectAsset(phAsset: PHAsset) -> Bool {
//        //Custom Rules & Display
//        //You can decide in which case the selection of the cell could be forbidden.
//    }
    func didExceedMaximumNumberOfSelection(picker: TLPhotosPickerViewController) {
        // exceed max selection
    }
    func handleNoAlbumPermissions(picker: TLPhotosPickerViewController) {
        // handle denied albums permissions case
    }
    func handleNoCameraPermissions(picker: TLPhotosPickerViewController) {
        // handle denied camera permissions case
    }

}
