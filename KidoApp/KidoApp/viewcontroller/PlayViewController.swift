//
//  PlayViewController.swift
//  DrawingBook
//
//  Created by monika kumari on 13/05/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import AVFoundation

//extension UIImageView {
//    func shake(_ duration: Double? = 0.4) {
//          self.transform = CGAffineTransform(translationX: 20, y: 0)
//          UIView.animate(withDuration: duration ?? 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
//              self.transform = CGAffineTransform.identity
//          }, completion: nil)
//      }
//    func vibrate(){
//        let animation = CABasicAnimation(keyPath: "position")
//        animation.duration = 0.05
//        animation.repeatCount = 5
//        animation.autoreverses = true
//        animation.fromValue = NSValue(cgPoint: CGPoint(x:self.center.x - 2.0, y:self.center.y))
//        animation.toValue = NSValue(cgPoint: CGPoint(x:self.center.x + 2.0, y:self.center.y))
//        self.layer.add(animation, forKey: "position")
//    }
//        func startRotating(duration: CFTimeInterval = 3, repeatCount: Float = Float.infinity, clockwise: Bool = true) {
//
//            if self.layer.animation(forKey: "transform.rotation.z") != nil {
//                return
//            }
//
//            let animation = CABasicAnimation(keyPath: "transform.rotation.z")
//            let direction = clockwise ? 1.0 : -1.0
//            animation.toValue = NSNumber(value: .pi * 2 * direction)
//            animation.duration = duration
//            animation.isCumulative = true
//            animation.repeatCount = repeatCount
//            self.layer.add(animation, forKey:"transform.rotation.z")
//        }
//
//        func stopRotating() {
//
//            self.layer.removeAnimation(forKey: "transform.rotation.z")
//
//        }
//
//    }

@IBDesignable
class RoundButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
        self.layer.cornerRadius = cornerRadius
        }
    }

    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            self.layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor = borderColor.cgColor
        }
    }
}

extension UIImage {
    func withBackground(color: UIColor, opaque: Bool = true) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, opaque, scale)

        guard let ctx = UIGraphicsGetCurrentContext() else { return self }
        defer { UIGraphicsEndImageContext() }

        let rect = CGRect(origin: .zero, size: size)
        ctx.setFillColor(color.cgColor)
        ctx.fill(rect)
        ctx.concatenate(CGAffineTransform(a: 1, b: 0, c: 0, d: -1, tx: 0, ty: size.height))
        ctx.draw(cgImage!, in: rect)

        return UIGraphicsGetImageFromCurrentImageContext() ?? self
    }
}

class PlayViewController: BaseViewController  {
    /// Variables
       var originalRect: CGRect!
       @IBOutlet weak var textLabel: UILabel!
    @IBOutlet var detailButton: RoundButton!
    @IBOutlet var puzzleButton: RoundButton!

    var randomImageName:String = String()

    @IBOutlet var spellV: UIView!
    var alphabetsImages:[String] = ["apple","ball","cat","dog","elephant","fish","giraffe","horse","ice-cream","jug","kite","lion","mango","nest","octopus","parrot","queen","rabbit","sun","tortoise","umbrella","vase","watch","x-mas tree","yak","zebra"]
    var numberText:[String] = ["one","two","three","four","five","six","seven","eight","nine","ten"]


    @IBOutlet var prevButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    
    
    @IBOutlet var bottomV: UIStackView!
    
    
//    var optOneLbl = UILabel()
    @IBOutlet weak var animalNameLabel: UILabel!
    @IBOutlet weak var playStopButton: UIButton!

    @IBOutlet var ImgV: UIImageView!
    @IBOutlet var customV: UIView!

    @IBOutlet var bucketImgV: UIImageView!

//    @IBOutlet weak var ImgV = UIImageView()!

    var selectedIndex = Int()
    var selecteditem:String = String()

    var selecteditemName:String = String()
    var selecteditemImage:UIImage = UIImage()


  var selectedArray = ["CAT", "RAT", "PIG", "COW", "LION", "ROOSTER", "DOG", "MONKEY"]
  var selectedArrayImage = ["cat", "rat", "pig", "cow", "lion", "rooster", "dog", "monkey"]

    var isPlaying:Bool = false
    var isRotating:Bool = false
    static let numRows = 5
       static let numColumns = 7
    var objPlayer: AVAudioPlayer?
    
    func refreshArrayAndUpdateUI()  {
        if selectedArrayImage.count > 0{
            selecteditemImage = UIImage(named: selectedArrayImage[selectedIndex])!
               ImgV.image = selecteditemImage
            selecteditemName = selectedArray[selectedIndex]
                          animalNameLabel.text = selecteditemName
    }
        else{
            selecteditemName = selectedArray[selectedIndex]

            textLabel.text = selecteditemName
            if selecteditem == learnListItems.alphabets.rawValue{
                            animalNameLabel.text = "\(textLabel.text!) for \(alphabetsImages[selectedIndex].uppercased())"

            }
            else{
                animalNameLabel.text = numberText[selectedIndex].uppercased()
            }
            

            
        }
              
        
    }
    @objc func nextClicked(_ sender: UIButton) {
         sender.flash()
    self.buttonClickedPlayer?.play()
        if (selectedIndex + 1) < selectedArray.count {
            selectedIndex = selectedIndex + 1
            
            refreshArrayAndUpdateUI()

        }
        else{
            selectedIndex = 0
            refreshArrayAndUpdateUI()

        }
        spellContent()

        
    }
    @objc func prevClicked(_ sender: UIButton) {
         sender.flash()
    self.buttonClickedPlayer?.play()
        if (selectedIndex - 1) > 0 {

        selectedIndex = selectedIndex - 1
         refreshArrayAndUpdateUI()
        }
        else{
            selectedIndex = 0
              refreshArrayAndUpdateUI()
        }
        spellContent()

        
    }
    func addCustomView()  {
       
        
                   customV.backgroundColor = .clear

        textLabel.textAlignment = .center
        textLabel.textColor = UIColor.white
        textLabel.font = Utility.fontWithSize(size: (150 * Utility.getScreenScaleFactor()))

        textLabel.text = selecteditemName
             let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.TappedMethod(_:)))

                    customV.isUserInteractionEnabled = true
                  customV.addGestureRecognizer(tapGestureRecognizer)
        

               
    }
    func setUpanimalLabel()  {
            animalNameLabel.layer.cornerRadius = 10
               animalNameLabel.layer.borderWidth = 5
               animalNameLabel.layer.borderColor = UIColor.black.cgColor
               animalNameLabel.layer.masksToBounds = true
    }
    func addImageView()  {
        ImgV = UIImageView(frame: CGRect(x: 10, y: 200, width: (self.view.bounds.width) - 20, height: (self.view.frame.height/2)))
               ImgV.contentMode = .scaleAspectFit
                   ImgV.backgroundColor = .clear
                   self.view.addSubview(ImgV)
             let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.TappedMethod(_:)))

                    ImgV.isUserInteractionEnabled = true
                  ImgV.addGestureRecognizer(tapGestureRecognizer)
        self.playStopButton.sendSubviewToBack(ImgV)

               
    }
    func setUpNavigationBar()  {
                     self.navigationController?.navigationBar.isHidden=false
                       let image = UIImage.fontAwesomeIcon(name: .volumeDown, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
                       let volumeBtn = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.volumeButtonPressed(sender:)))
                       self.navigationItem.rightBarButtonItem  = volumeBtn
                       
                }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        setUpNavigationBar()
        self.setupfontAwesomeIconToButton(button: detailButton, iconname: .streetView, fontsize:(50 * Utility.getScreenScaleFactor()), color: .white)
       detailButton.addTarget(self, action: #selector(self.viewButtonPressed(sender:)), for: .touchUpInside)
        self.setupfontAwesomeIconToButton(button: puzzleButton, iconname: .puzzlePiece, fontsize:(50 * Utility.getScreenScaleFactor()), color: .white)


        self.setupNextButtontoView()
        self.setupPrevButtontoView()
        self.navigationController?.navigationBar.isHidden=false
        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])

        // Do any additional setup after loading the view, typically from a nib.
        ImgV.contentMode = .scaleAspectFit
        ImgV.backgroundColor = .clear
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.TappedMethod(_:)))

                          ImgV.isUserInteractionEnabled = true
                        ImgV.addGestureRecognizer(tapGestureRecognizer)
        switch selecteditem {
        case learnListItems.animals.rawValue:
            ImgV.isHidden = false
            bucketImgV.isHidden = false
            ImgV.image = selecteditemImage
            animalNameLabel.text = selecteditemName
            customV.isHidden = true

            case learnListItems.birds.rawValue:
                       ImgV.isHidden = false
                       bucketImgV.isHidden = false
                       ImgV.image = selecteditemImage
            animalNameLabel.text = selecteditemName
             customV.isHidden = true

            
            case learnListItems.instruments.rawValue:
                       ImgV.isHidden = false
                       bucketImgV.isHidden = false
                       animalNameLabel.text = selecteditemName
                        customV.isHidden = true

            ImgV.image = selecteditemImage

            case learnListItems.alphabets.rawValue:
                       addCustomView()
                       alphabetsImages = ["apple","ball","cat","dog","elephant","fish","giraffe","horse","ice-cream","jug","kite","lion","mango","nest","octopus","parrot","queen","rabbit","sun","tortoise","umbrella","vase","watch","x-mas tree","yak","zebra"]
                       animalNameLabel.text = "\(textLabel.text!) for \(alphabetsImages[selectedIndex].uppercased())"
                       ImgV.isHidden = true
                       bucketImgV.isHidden = false
                        customV.isHidden = false
                       animalNameLabel.isHidden = false
            case learnListItems.numbers.rawValue:

                                customV.isHidden = false
                                 addCustomView()
                                 ImgV.isHidden = true
                                 bucketImgV.isHidden = false
                                 animalNameLabel.isHidden = false
                animalNameLabel.text = numberText[selectedIndex].uppercased()


        default:
            addCustomView()

        }
        
        setUpanimalLabel()
       

       



    }
    func moveToNumberPuzzle()  {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NumberPuzzleViewController") as? NumberPuzzleViewController
              vc?.item = self.selecteditemName
        vc?.index = self.selectedIndex

         self.navigationController?.pushViewController(vc!, animated: true)
    }
    func MoveToRainingVC()  {
           let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "RainingViewController") as? RainingViewController
                   vc?.item = self.selecteditemName
           vc?.index = self.selectedIndex
        self.buttonClickedPlayer?.play()

            self.navigationController?.pushViewController(vc!, animated: true)
           
           
            }
    func MoveToBubbleVC()  {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BubbleViewController") as? BubbleViewController
                vc?.item = self.selecteditemName
        vc?.index = self.selectedIndex
        vc?.selecteditem = self.selecteditem
         self.navigationController?.pushViewController(vc!, animated: true)
        
        
         }
    func makePuzzle()  {
       let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "PuzzleCollectionViewController") as? PuzzleCollectionViewController
        vc?.puzzleTittle = self.animalNameLabel.text?.lowercased() as! String
        self.navigationController?.pushViewController(vc!, animated: true)
        }
    @objc func TappedMethod(_ sender:AnyObject){
        let zoomableVC = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ZoomableViewController") as! ZoomableViewController

     self.buttonClickedPlayer?.play()

        switch selecteditem {
               case learnListItems.animals.rawValue:
                zoomableVC.item = self.animalNameLabel.text!

                        zoomableVC.getImage = ImgV.image
                        zoomableVC.selecteditem = self.selecteditem
                                 self.navigationController?.pushViewController(zoomableVC, animated: true)

                   case learnListItems.birds.rawValue:
                    zoomableVC.item = self.animalNameLabel.text!

                                 zoomableVC.getImage = ImgV.image
                                            zoomableVC.selecteditem = self.selecteditem
                                           self.navigationController?.pushViewController(zoomableVC, animated: true)
                   
                   case learnListItems.instruments.rawValue:
                    zoomableVC.item = self.animalNameLabel.text!

                                  zoomableVC.getImage = ImgV.image
                                            zoomableVC.selecteditem = self.selecteditem
                                           self.navigationController?.pushViewController(zoomableVC, animated: true)


                   case learnListItems.alphabets.rawValue:
                    zoomableVC.item = self.animalNameLabel.text!
                                                zoomableVC.index = self.selectedIndex
                                                  zoomableVC.selecteditem = self.selecteditem
                                                  self.navigationController?.pushViewController(zoomableVC, animated: true)

                   case learnListItems.numbers.rawValue:
                                                    zoomableVC.item = self.selecteditemName
                                                    zoomableVC.index = self.selectedIndex
                                                               zoomableVC.selecteditem = self.selecteditem
                                                              self.navigationController?.pushViewController(zoomableVC, animated: true)


               default:
                self.makePuzzle()
               }
//          if isPlaying{
//
//              isPlaying = false
//            objPlayer?.stop()
//            let img = UIImage(named: "play")
//                   playStopButton.setImage(img , for: .normal)
//    //        sender.setTitle("STOP", for: UIControl.State.normal)
//              //Pause Stopwatch
//          }
//          else{
//
//              isPlaying = true
//            let audiofilename:String = selectedArrayImage[selectedIndex]
//
//            self.playAudioFile(audioName: audiofilename)
//
//
//
//
//
//    //          sender.setTitle("PLAY", for:  UIControl.State.normal)
//            //Play Stopwatch
//          }
        }
    func playAudioFile(audioName:String) {
        guard let url = Bundle.main.url(forResource: audioName, withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try AVAudioSession.sharedInstance().setActive(true)

            // For iOS 11
            objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            // For iOS versions < 11
            objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            guard let aPlayer = objPlayer else { return }
            aPlayer.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
   
    
   
    func addZoombehaviour()  {
        originalRect = customV.frame
                          customV.frame = originalRect
        collisionBehavior = UICollisionBehavior(items: [customV])
                   collisionBehavior.translatesReferenceBoundsIntoBoundary = true
                   
        dynamicAnimator = UIDynamicAnimator(referenceView: view)
                        
                  gravityBehavior  = UIGravityBehavior(items: [customV])
                  dynamicAnimator.addBehavior(gravityBehavior)
                  gravityBehavior.gravityDirection = CGVector(dx: 0, dy: 1)
         dynamicAnimator.addBehavior(collisionBehavior)
                   
                   bouncingBehavior = UIDynamicItemBehavior(items: [customV])
                   bouncingBehavior.elasticity = 0.70
                   dynamicAnimator.addBehavior(bouncingBehavior)

       }
    
    func addjumpbehaviour()  {
        originalRect = ImgV.frame
         ImgV.frame = originalRect
        self.addGravity(toImageV: ImgV)
        self.addBounce(toImageV: ImgV)
        
    }
    @IBAction func puzzleButtonClicked(_ sender: RoundButton) {
         sender.flash()
    self.buttonClickedPlayer?.play()
        //self.makePuzzle()
        switch selecteditem {
               case learnListItems.animals.rawValue:
                   self.makePuzzle()

                   case learnListItems.birds.rawValue:
                             self.makePuzzle()
                   
                   case learnListItems.instruments.rawValue:
                             self.makePuzzle()


                   case learnListItems.alphabets.rawValue:
                                    self.MoveToRainingVC()

                   case learnListItems.numbers.rawValue:
                    self.moveToNumberPuzzle()


               default:
                self.makePuzzle()
               }
        
        
    }
    
    @objc func viewButtonPressed(sender:UIButton){
               sender.flash()
    self.buttonClickedPlayer?.play()
        self.MoveToBubbleVC()

              //spellContent()
          }
    @objc func volumeButtonPressed(sender:UIBarButtonItem){
        // sender.flash()
    self.buttonClickedPlayer?.play()
           spellContent()
       }
    func setupNextButtontoView()  {
               nextButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 30, style: .solid)
                 nextButton.setTitle(String.fontAwesomeIcon(name: .stepForward), for: .normal)
                 nextButton.setTitleColor(UIColor.white, for: .normal)
                 nextButton.addTarget(self, action: #selector(self.nextClicked(_:)), for: .touchUpInside)
             }
    func setupPrevButtontoView()  {
            prevButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 30, style: .solid)
              prevButton.setTitle(String.fontAwesomeIcon(name: .stepBackward), for: .normal)
              prevButton.setTitleColor(UIColor.white, for: .normal)
              prevButton.addTarget(self, action: #selector(self.prevClicked(_:)), for: .touchUpInside)
          }
    
    func spellContent()  {
              switch selecteditem {
        case learnListItems.animals.rawValue:
            let animal:String = selecteditemName.lowercased()
            self.speakgivenSpeech(speech: animal)


            case learnListItems.birds.rawValue:
                     let bird:String = selecteditemName.lowercased()
                      self.speakgivenSpeech(speech: bird)

            
            case learnListItems.instruments.rawValue:
                      let instrument:String = selecteditemName.lowercased()
                      self.speakgivenSpeech(speech: instrument)



            case learnListItems.alphabets.rawValue:
              let alphabet:String = textLabel.text!.lowercased()
                             self.speakgivenSpeech(speech: alphabet)


            case learnListItems.numbers.rawValue:
                                         let number:String = textLabel.text!.lowercased()
                                         self.speakgivenSpeech(speech: number)


        default:
        print("")
        }
    }
    @IBAction func detailButtonClicked(_ sender: RoundButton) {
            sender.flash()
    self.buttonClickedPlayer?.play()
        spellContent()
        
           
       }
//    func setUIForEnabled(tappedButton:UIButton){
//           tappedButton.alpha = 1
//       }
//    func setUIForDisabled(tappedButton:UIButton){
//        tappedButton.alpha = 0.5
//    }
//
    
    
    
    @IBAction func btnStartStop(sender: UIButton) {
      if isPlaying{
        
          isPlaying = false
        objPlayer?.stop()
        let img = UIImage(named: "play")
               sender.setImage(img , for: .normal)
        switch selecteditem {
          case learnListItems.animals.rawValue:
                          ImgV.frame = originalRect
        dynamicAnimator.removeAllBehaviors()


              case learnListItems.birds.rawValue:
                                     ImgV.frame = originalRect
        dynamicAnimator.removeAllBehaviors()


              
              case learnListItems.instruments.rawValue:
                                    ImgV.frame = originalRect
        dynamicAnimator.removeAllBehaviors()




              case learnListItems.alphabets.rawValue:
                                    customV.frame = originalRect
        dynamicAnimator.removeAllBehaviors()


              case learnListItems.numbers.rawValue:
                                              customV.frame = originalRect
        dynamicAnimator.removeAllBehaviors()


          default:
              ImgV.frame = originalRect

          }
//        sender.setTitle("STOP", for: UIControl.State.normal)
          //Pause Stopwatch
      }
      else{

          isPlaying = true
   
        let img = UIImage(named: "stop")
                      sender.setImage(img , for: .normal)
        switch selecteditem {
        case learnListItems.animals.rawValue:
            let audiofilename:String = selectedArrayImage[selectedIndex]

                                 self.playAudioFile(audioName: audiofilename)
                  
                        self.addjumpbehaviour()

            case learnListItems.birds.rawValue:
                let jaildoor:String = "jail_cell_door"
                 self.playAudioFile(audioName: jaildoor)
                      
                                   self.addjumpbehaviour()

            
            case learnListItems.instruments.rawValue:
                let jaildoor:String = "jail_cell_door"
                                self.playAudioFile(audioName: jaildoor)
                                     
                      
                                  self.addjumpbehaviour()



            case learnListItems.alphabets.rawValue:
                 let jaildoor:String = "jail_cell_door"
                                self.playAudioFile(audioName: jaildoor)
                                     
                      
                                  self.addZoombehaviour()

            case learnListItems.numbers.rawValue:
                let jaildoor:String = "jail_cell_door"
                                self.playAudioFile(audioName: jaildoor)
                                     
                                            self.addZoombehaviour()

        default:
            self.addjumpbehaviour()

        }

//          sender.setTitle("PLAY", for:  UIControl.State.normal)
        //Play Stopwatch
      }
    }
    
    
    
//       @IBAction func playButtonClicked(_ sender: RoundButton) {
//         sender.flash()
//        if sender.isEnabled == true {
//            sender.isEnabled = false
//            self.setUIForDisabled(tappedButton: sender)
//
//        }
//        else{
//            sender.isEnabled = true
//            self.setUIForEnabled(tappedButton: sender)
//            playAudioFile()
//
//
//        }
//       }
       @IBAction func stopButtonClicked(_ sender: RoundButton) {
        objPlayer?.stop()
       }
//    func fireAnimation(toY:CGFloat) {
//        UIView.animate(withDuration: 12.0, delay: 0.0, options: [ .allowUserInteraction, .curveLinear, .autoreverse, .repeat ], animations: {
//            self.ImgV.center = CGPoint(x: self.ImgV.center.x, y:toY)
//        })
//    }



   /* override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let touchLocation = touch!.location(in: self.view)

        if self.ImgV.layer.presentation()!.hitTest(touchLocation) != nil {
            //re
            ImgV.layer.removeAllAnimations()
            ImgV.center = touchLocation
            
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let touchLocation = touch!.location(in: self.view)
        var sender: UIImageView

        if self.ImgV.layer.presentation()!.hitTest(touchLocation) != nil {
            self.ImgV.center = touchLocation
            sender = self.ImgV
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        //restart animation after finished and change the Y if you want.
        // you could change duration or whatever
//        fireAnimation(toY: self.view.bounds.height - ImgV.bounds.height)
    }*/

}
