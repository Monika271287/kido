//
//  DrawingViewController.swift
//  DrawingBook
//
//  Created by monika kumari on 03/01/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import  JTMaterialTransition

//}
class DrawingViewController: BaseViewController{
  
  @IBOutlet weak var mainImageView: UIImageView!
  @IBOutlet weak var tempImageView: UIImageView!
  
  var lastPoint = CGPoint.zero
  var color = UIColor.black
  var brushWidth: CGFloat = 10.0
  var opacity: CGFloat = 1.0
  var swiped = false

  @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var blackPencilButton: UIButton!
    @IBOutlet weak var grayPencilButton: UIButton!
    @IBOutlet weak var bluePencilButton: UIButton!

    @IBOutlet weak var skybluePencilButton: UIButton!
    @IBOutlet weak var darkgreenPencilButton: UIButton!
    @IBOutlet weak var lightgreenPencilButton: UIButton!

    @IBOutlet weak var brownPencilButton: UIButton!
    @IBOutlet weak var orangePencilButton: UIButton!
    @IBOutlet weak var yellowPencilButton: UIButton!
    @IBOutlet weak var eraserPencilButton: UIButton!

//  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//    guard let navController = segue.destination as? UINavigationController,
//      let settingsController = navController.topViewController as? SettingsViewController else {
//        return
//    }
//    settingsController.delegate = self
//    settingsController.brush = brushWidth
//    settingsController.opacity = opacity
//
//    var red: CGFloat = 0
//    var green: CGFloat = 0
//    var blue: CGFloat = 0
//    color.getRed(&red, green: &green, blue: &blue, alpha: nil)
//    settingsController.red = red
//    settingsController.green = green
//    settingsController.blue = blue
//  }
    @IBAction func backClicked(_ sender: UIButton) {
                   self.dismiss(animated: true, completion: nil)

       }
  
  
    
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        self.setupfontAwesomeIconToButton(button: settingsButton, iconname: .alignJustify, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)
        self.setupfontAwesomeIconToButton(button: resetButton, iconname: .undo, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)
        self.setupfontAwesomeIconToButton(button: shareButton, iconname: .share, fontsize:(30 * Utility.getScreenScaleFactor()),color:.white)
        
        self.setupfontAwesomeIconToButton(button: redButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 1, green: 0, blue: 0, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: blackPencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:.black)
        self.setupfontAwesomeIconToButton(button: grayPencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(white: 105/255.0, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: bluePencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 0, green: 0, blue: 1, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: skybluePencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 51/255.0, green: 204/255.0, blue: 1, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: darkgreenPencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 102/255.0, green: 204/255.0, blue: 0, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: lightgreenPencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 102/255.0, green: 1, blue: 0, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: brownPencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 160/255.0, green: 82/255.0, blue: 45/255.0, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: orangePencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 1, green: 102/255.0, blue: 0, alpha: 1.0))
        self.setupfontAwesomeIconToButton(button: yellowPencilButton, iconname: .pen, fontsize:(50 * Utility.getScreenScaleFactor()),color:UIColor(red: 1, green: 1, blue: 0, alpha: 1.0))

       backButton.setBackgroundImage(UIImage(named: "back")?.maskWithColor(color: UIColor.orange), for: .normal)
       backButton.setBackgroundImage(UIImage(named: "back")?.maskWithColor(color: UIColor.orange), for: .normal)


        

    }
  // MARK: - Actions
  @IBAction func settingsPressed(_ sender: UIButton) {
     sender.flash()
    self.buttonClickedPlayer?.play()
   self.transition = JTMaterialTransition(animatedView: self.settingsButton)

   let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
   //self.navigationController?.pushViewController(vc!, animated: true)
   vc?.modalPresentationStyle = .custom
         vc?.transitioningDelegate = self.transition
    vc?.delegate = self
       vc?.brush = brushWidth
       vc?.opacity = opacity
       
       var red: CGFloat = 0
       var green: CGFloat = 0
       var blue: CGFloat = 0
       color.getRed(&red, green: &green, blue: &blue, alpha: nil)
       vc?.red = red
       vc?.green = green
       vc?.blue = blue
                
         self.present(vc!, animated: true, completion: nil)
  }
  @IBAction func resetPressed(_ sender: UIButton) {
     sender.flash()
    self.buttonClickedPlayer?.play()
    mainImageView.image = nil
  }
  
  @IBAction func sharePressed(_ sender: UIButton) {
     sender.flash()
    self.buttonClickedPlayer?.play()
    guard let image = mainImageView.image else {
      return
    }
    let activity = UIActivityViewController(activityItems: [image], applicationActivities: nil)
    present(activity, animated: true)
  }
  
  @IBAction func pencilPressed(_ sender: UIButton) {
     sender.flash()
    self.buttonClickedPlayer?.play()
    guard let pencil = Pencil(tag: sender.tag) else {
      return
    }
    
    color = pencil.color
    if pencil == .eraser {
      opacity = 1.0
    }
  }
  
  func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
    UIGraphicsBeginImageContext(view.frame.size)
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    tempImageView.image?.draw(in: view.bounds)
    
    context.move(to: fromPoint)
    context.addLine(to: toPoint)
    
    context.setLineCap(.round)
    context.setBlendMode(.normal)
    context.setLineWidth(brushWidth)
    context.setStrokeColor(color.cgColor)
    
    context.strokePath()
    
    tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
    tempImageView.alpha = opacity
    
    UIGraphicsEndImageContext()
  }
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else {
      return
    }
    swiped = false
    lastPoint = touch.location(in: view)
  }
  
  override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    guard let touch = touches.first else {
      return
    }
    swiped = true
    let currentPoint = touch.location(in: view)
    drawLine(from: lastPoint, to: currentPoint)
    
    lastPoint = currentPoint
  }
  
  override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    if !swiped {
      // draw a single point
      drawLine(from: lastPoint, to: lastPoint)
    }
    
    // Merge tempImageView into mainImageView
    UIGraphicsBeginImageContext(mainImageView.frame.size)
    mainImageView.image?.draw(in: view.bounds, blendMode: .normal, alpha: 1.0)
    tempImageView?.image?.draw(in: view.bounds, blendMode: .normal, alpha: opacity)
    mainImageView.image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    tempImageView.image = nil
  }
}

// MARK: - SettingsViewControllerDelegate

extension DrawingViewController: SettingsViewControllerDelegate {
  func settingsViewControllerFinished(_ settingsViewController: SettingsViewController) {
    brushWidth = settingsViewController.brush
    opacity = settingsViewController.opacity
    color = UIColor(red: settingsViewController.red,
                    green: settingsViewController.green,
                    blue: settingsViewController.blue,
                    alpha: opacity)
    dismiss(animated: true)
  }
}

   // let canvas = Canvas()
//    @IBOutlet weak var backButton: UIButton!
//
//    override func viewDidLoad() {
//          super.viewDidLoad()
//        self.navigationController?.navigationBar.isHidden=false
//
//        view.addSubview(canvas)
//        backButton.setBackgroundImage(UIImage(named: "back")?.maskWithColor(color: UIColor.blue), for: .normal)
//
//        canvas.backgroundColor = .white
//        canvas.frame = CGRect(x: 0, y: 120, width: ScreenSize.SCREEN_WIDTH, height: ScreenSize.SCREEN_HEIGHT)
//    }
  
    

