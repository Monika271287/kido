/// Copyright (c) 2019 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import CoreGraphics

enum ImageName {
    
    static let cloud = "cloud"

    static let background = "background"
    static let background_farm = "farmbackground"
    static let boy = "boy"
    static let ball = "ball"
    static let bird = "bird"


  static let tree = "tree"
    static let appletree = "appletree"

  static let ground = "ground"
  static let water = "Water"
  static let vineTexture = "VineTexture"
  static let vineHolder = "VineHolder"
  static let crocMouthClosed = "CrocMouthClosed"
  static let crocMouthOpen = "CrocMouthOpen"
  static let crocMask = "CrocMask"
  static let prize = "Pineapple"
  static let prizeMask = "PineappleMask"
    static let sheepleft = "sheeptoleft"
    static let sheepright = "sheeptoright"
    static let sheep = "sheep"

    static let yellowsheep = "yellowSheep"
    static let spider = "spider"
    static let incyup = "incyup"
    static let incydown = "incydown"

    static let ship = "ship"
    static let shipright = "shiptoright"
    static let bingoleft = "dogtoleft"
    static let bingoright = "dogtoright"
    static let farmer1 = "farmer1"
    static let farmer2 = "farmer2"

    static let grass = "grass"
    static let sunright = "sunright"
    static let sunleft = "sunleft"
    static let waterspout = "waterspout"

    static let boatright = "boatright"
       static let boatleft = "boatleft"
    static let oceanright = "oceanright"
    static let oceanleft = "oceanleft"


}

enum SoundFile {
  static let backgroundMusic = "CheeZeeJungle"
  static let slice = "Slice.caf"
  static let splash = "Splash.caf"
  static let nomNom = "NomNom.caf"
    static let baba = "baba"
    static let bingo = "bingo"
    static let insy = "insy"
    static let rowyourboat = "rowyourboat"


}

enum Layer {
    static let sky: CGFloat = -30

  static let background: CGFloat = 0
  static let crocodile: CGFloat = 1
  static let vine: CGFloat = 1
  static let prize: CGFloat = 2
  static let foreground: CGFloat = 3
    static let top: CGFloat = 4
    static let hero: CGFloat = 1
    static let spider: CGFloat = 5
    static let farmer: CGFloat = 6
    static let sun: CGFloat = -20

    static let cloud: CGFloat = -10



}

enum PhysicsCategory {
  static let crocodile: UInt32 = 1
  static let vineHolder: UInt32 = 2
  static let vine: UInt32 = 4
  static let prize: UInt32 = 8
    static let hero: UInt32 = 8
    static let spider: UInt32 = 9

}

enum GameConfiguration {
  static let vineDataFile = "VineData.plist"
    static let vineDataSpiderFile = "VineDataSpider.plist"

  static let canCutMultipleVinesAtOnce = false
}

enum Scene {
  static let particles = "Particle.sks"
}
