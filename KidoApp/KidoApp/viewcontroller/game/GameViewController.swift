//
//  GameViewController.swift
//  KidoApp
//
//  Created by monika kumari on 19/08/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//
import UIKit
import SpriteKit
import GameplayKit
protocol GameViewControllerDelegate:class {
    func movetoLearnlistFromGame(game:Int,tocontroller:UIViewController)
}
class GameViewController: BaseViewController {
    @IBOutlet var skView: SKView!
    var gamenumber:Int = 0
    var type:learnListType = learnListType.play
    var gametype:gameListType = gameListType.feedcrocodile

    var delegate:GameViewControllerDelegate? //declare a delegate

     @IBAction func backClicked(_ sender: UIButton) {
        MusicManager.shared.stop()

        self.dismiss(animated: true, completion: nil)
         }
    
    
  override func viewDidLoad() {
    super.viewDidLoad()
    MusicManager.shared.setup(resourceName: SoundFile.backgroundMusic, oftype: "caf")
    MusicManager.shared.play()

    self.view.backgroundColor = UIColor.black
    skView.showsFPS = true
    skView.showsNodeCount = true
    skView.ignoresSiblingOrder = true
    // Create and configure the scene.
    let scene = GameScene(size: CGSize(width: skView.frame.width, height: skView.frame.height))
//    scene.scaleMode = .aspectFill

    // Present the scene.
    skView.presentScene(scene)
  }
}
