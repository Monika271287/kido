//
//  StoryRhymeScene.swift
//  KidoApp
//
//  Created by monika kumari on 27/08/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import SpriteKit
extension SKSpriteNode {

    func addGlow(radius: Float = 30) {
        let effectNode = SKEffectNode()
        effectNode.shouldRasterize = true
        addChild(effectNode)
        effectNode.addChild(SKSpriteNode(texture: texture))
        effectNode.filter = CIFilter(name: "CIGaussianBlur", parameters: ["inputRadius":radius])
    }
}
extension SKLabelNode{
    func startTyping(_ duration:TimeInterval, completion:(()->Void)?){
        guard let text = self.text else{return}

        self.text = ""
        var index = 0
        var block:(() -> Void)!
        block = {
            index += 1
            if index > text.count{
                completion?()
                return
            }else{
                let action = SKAction.sequence([SKAction.wait(forDuration: duration), SKAction.run{self.text = String(text.prefix(index))}])
                self.run(action, completion: block)
            }
        }
        block()
    }
}
class StoryRhymeScene: SKScene,SKPhysicsContactDelegate {
    private var spider: SKSpriteNode!
  let dropTexture = SKTexture(imageNamed: "drop")


    var rhymename:RhymeListType  = RhymeListType(rawValue: 0)!
    var learntype:learnListType = learnListType(rawValue: 0)!
    
    func makeBouncer(at position: CGPoint) {
        let bouncer = SKSpriteNode(imageNamed: "ballon")
        bouncer.position = position
        bouncer.physicsBody = SKPhysicsBody(circleOfRadius: bouncer.size.width / 2.0)
        bouncer.physicsBody?.isDynamic = false
        addChild(bouncer)
    }
    func makeSlot(at position: CGPoint, isGood: Bool) {
        var slotBase: SKSpriteNode
        var slotGlow: SKSpriteNode

        if isGood {
            slotBase = SKSpriteNode(imageNamed: "apple")
            slotGlow = SKSpriteNode(imageNamed: "pineapple")
            slotBase.name = "good"
        } else {
            slotBase = SKSpriteNode(imageNamed: "fish")
            slotGlow = SKSpriteNode(imageNamed: "tree")
            slotBase.name = "bad"
        }
        slotBase.position = position
        slotGlow.position = position
        slotBase.physicsBody = SKPhysicsBody(rectangleOf: slotBase.size)
        slotBase.physicsBody?.isDynamic = false
        addChild(slotBase)
        addChild(slotGlow)
     
        
    }
    func addrain()  {
       /* Load our particle effect */
//       let particles = SKEmitterNode(fileNamed: "Poof")!
//       /* Position particles at the Seal node
//         If you've moved Seal to an sks, this will need to be
//         node.convert(node.position, to: self), not node.position */
//       /* Add particles to scene */
//       addChild(particles)
//       let wait = SKAction.wait(forDuration: 5)
//       let removeParticles = SKAction.removeFromParent()
//       let seq = SKAction.sequence([wait, removeParticles])
//       particles.run(seq)
    }
   
    func animatewith2image(position:CGPoint,leftImagename:String,rightImagename:String,size:CGSize,zposition:CGFloat,anchotpt:CGPoint) {
        let sunTexture = SKTexture(imageNamed:rightImagename )
                let node = SKSpriteNode(texture: sunTexture)
        node.zPosition = zposition
        node.position = position
        node.size =  size
        node.anchorPoint = anchotpt

                 addChild(node)
//       let oneRevolution:SKAction = SKAction.rotate(byAngle: CGFloat.pi * 2, duration: 1)
//        let repeatRotation:SKAction = SKAction.repeatForever(oneRevolution)
//
//        node.run(repeatRotation)
        let imageleft = SKAction.setTexture(SKTexture(imageNamed: leftImagename))
            let wait = SKAction.wait(forDuration: 1)

            let imageright = SKAction.setTexture(SKTexture(imageNamed:rightImagename))
            let sequence = SKAction.sequence([imageleft,wait,imageright,wait])
                
            node.run(.repeatForever(sequence))
       

    }
    func createcloud() {
        let groundTexture = SKTexture(imageNamed: ImageName.cloud)

        for i in 0 ... 1 {
            let ground = SKSpriteNode(texture: groundTexture)
            ground.zPosition =  Layer.cloud
            debugPrint(groundTexture.size().height)
            ground.position = CGPoint(x: (groundTexture.size().width / 2.0 + (groundTexture.size().width * CGFloat(i))), y: (size.height) - ((groundTexture.size().height/2) ))

            addChild(ground)

            let moveLeft = SKAction.moveBy(x: -groundTexture.size().width, y: 0, duration: 5)
            let moveReset = SKAction.moveBy(x: groundTexture.size().width, y: 0, duration: 0)
            let moveLoop = SKAction.sequence([moveLeft, moveReset])
            let moveForever = SKAction.repeatForever(moveLoop)

            ground.run(moveForever)
        }
    }
  
    func addImageToPosion(position:CGPoint,imagename:String,size:CGSize,zposition:CGFloat) {
       

       let NodeTexture = SKTexture(imageNamed:imagename )
            let Node: SKSpriteNode = SKSpriteNode(texture: NodeTexture)
        Node.zPosition = zposition
           Node.position = position
        Node.size = size
           addChild(Node)

         
       }
//    func addfriendImage(imagename:String) {
//    let NodeTexture = SKTexture(imageNamed:imagename )
//        Node = SKSpriteNode(texture: NodeTexture)
//        Node.zPosition =  Layer.hero
//        Node.position = CGPoint(x: frame.width / 2, y: frame.height * 0.4)
//
//        addChild(Node)
//
//
//    }
    func createBackground(imagename:String) {
        let groundTexture = SKTexture(imageNamed: imagename)

        for i in 0 ... 1 {
            let ground = SKSpriteNode(texture: groundTexture)
            ground.zPosition = -10
            ground.position = CGPoint(x: (groundTexture.size().width / 2.0 + (groundTexture.size().width * CGFloat(i))), y: groundTexture.size().height / 2)

            addChild(ground)

            let moveLeft = SKAction.moveBy(x: -groundTexture.size().width, y: 0, duration: 5)
            let moveReset = SKAction.moveBy(x: groundTexture.size().width, y: 0, duration: 0)
            let moveLoop = SKAction.sequence([moveLeft, moveReset])
            let moveForever = SKAction.repeatForever(moveLoop)

            ground.run(moveForever)
        }
    }
    func createSky() {
        let topSky = SKSpriteNode(color: UIColor(hue: 0.55, saturation: 0.14, brightness: 0.97, alpha: 1), size: CGSize(width: frame.width, height: frame.height * 0.67))
        topSky.anchorPoint = CGPoint(x: 0.5, y: 1)

        let bottomSky = SKSpriteNode(color: UIColor(hue: 0.55, saturation: 0.16, brightness: 0.96, alpha: 1), size: CGSize(width: frame.width, height: frame.height * 0.33))
        bottomSky.anchorPoint = CGPoint(x: 0.5, y: 1)

        topSky.position = CGPoint(x: frame.midX, y: frame.height)
        bottomSky.position = CGPoint(x: frame.midX, y: bottomSky.frame.height)

        addChild(topSky)
        addChild(bottomSky)

        bottomSky.zPosition =  Layer.sky
        topSky.zPosition = Layer.sky
    }
    override func didMove(to view: SKView) {
        // Create shape node to use during mouse interaction
        switch rhymename {
        case .babablacksheep:
              setUpPhysics()
              createSky()
              createcloud()
              let Node = SKSpriteNode(imageNamed: ImageName.sheepright)
              addImageToPosion(position: CGPoint(x:size.width*0.5,y:size.height*0.4 ), imagename: ImageName.tree, size: CGSize(width: 300, height:300), zposition: Layer.crocodile)
              setUpNode(node: Node, position: CGPoint(x: 100, y: 100), zposition: Layer.hero)
              moveandanimatewith2image(leftImage: ImageName.sheepleft, rightImage: ImageName.sheepright, node: Node, toPoint:  CGPoint(x: 100, y: 100))

            
            case .bingo:
            setUpPhysics()
            createSky()
            createcloud()
            let farmerNode = SKSpriteNode(imageNamed: ImageName.farmer1)
            setUpNode(node: farmerNode, position: CGPoint(x: 100, y: size.height*0.4), zposition: Layer.crocodile)
            animateStillImage(leftImage: ImageName.farmer1, rightImage: ImageName.farmer2, node: farmerNode)
           let Node = SKSpriteNode(imageNamed: ImageName.sheepright)
            addImageToPosion(position: CGPoint(x:size.width*0.5,y:size.height*0.4 ), imagename: ImageName.appletree, size: CGSize(width: 300, height:300), zposition: Layer.crocodile)
            setUpNode(node: Node, position: CGPoint(x: 100, y: 100), zposition: Layer.hero)
            moveandanimatewith2image(leftImage: ImageName.bingoleft, rightImage: ImageName.bingoright, node: Node, toPoint:  CGPoint(x: 100, y: 100))
            
            
            case .insywinsyspider:
            setUpPhysics()
            createSky()
            createcloud()
            animatewith2image(position:CGPoint(x:size.width-120,y:size.height - (size.width*0.3 )),leftImagename:ImageName.sunleft,rightImagename:ImageName.sunright,size:CGSize(width: size.width*0.3, height:size.width*0.3),zposition:Layer.sun, anchotpt: CGPoint(x: 0.5, y: 0.5))
            addImageToPosion(position: CGPoint(x:size.width*0.5,y:size.height*0.3 ), imagename: ImageName.waterspout, size: CGSize(width: 300, height:size.height*0.6), zposition: Layer.crocodile)

           
            setUpspider()
//            setUpVines(centreImage: spider)
            
            case .rowyourboat:
            setUpPhysics()
            createSky()
            createcloud()
            animatewith2image(position:CGPoint(x: 0, y: 0),leftImagename:ImageName.oceanleft,rightImagename:ImageName.oceanright,size:CGSize(width: size.width, height: size.height * 0.8),zposition:Layer.crocodile, anchotpt: CGPoint(x: 0.0, y: 0.0))


            let boatNode = SKSpriteNode(imageNamed: ImageName.boatright)
//           setUpScenery(imgname: ImageName.oceanright)
            setUpNode(node: boatNode, position: CGPoint(x: 100, y: size.height*0.4), zposition: Layer.hero)
            moveandanimatewith2image(leftImage: ImageName.boatleft, rightImage: ImageName.boatright, node: boatNode, toPoint: CGPoint(x: 100, y: size.height*0.4))
            default:
            dLog(message: "")
            
        }
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
//        makeBouncer(at: CGPoint(x: 0, y: 0))
//        makeBouncer(at: CGPoint(x: 256, y: 0))
//        makeBouncer(at: CGPoint(x: 512, y: 0))
//        makeBouncer(at: CGPoint(x: 768, y: 0))
//        makeBouncer(at: CGPoint(x: 1024, y: 0))
//        makeSlot(at: CGPoint(x: 128, y: 0), isGood: true)
//        makeSlot(at: CGPoint(x: 384, y: 0), isGood: false)
//        makeSlot(at: CGPoint(x: 640, y: 0), isGood: true)
//        makeSlot(at: CGPoint(x: 896, y: 0), isGood: false)
        physicsWorld.contactDelegate = self
//    let bouncer = SKSpriteNode(imageNamed: "bouncer")
//    bouncer.position = CGPoint(x: 512, y: 0)
//    bouncer.physicsBody = SKPhysicsBody(circleOfRadius: bouncer.size.width / 2.0)
//    bouncer.physicsBody?.isDynamic = false
//    addChild(bouncer)

      
    }
    
    private func setUpspider() {
       spider = SKSpriteNode(imageNamed: ImageName.incydown)
       spider.position = CGPoint(x: size.width * 0.5, y: 100)
       spider.zPosition = Layer.spider
       addChild(spider)
        let imageup = SKAction.setTexture(SKTexture(imageNamed: ImageName.incyup))

            let imagedown = SKAction.setTexture(SKTexture(imageNamed:ImageName.incydown))
        let wait = SKAction.wait(forDuration: 1)
        let moveUp = SKAction.moveBy(x: 0, y: size.height*0.3, duration: 2)
        let sequence = SKAction.sequence([imageup,moveUp,wait,imagedown,moveUp.reversed(),wait])

        spider.run(SKAction.repeatForever(sequence), withKey:  "moving")
     }
     
     //MARK: - Vine methods
     
    private func setUpVines(centreImage:SKSpriteNode) {
       // load vine data
       let decoder = PropertyListDecoder()
       guard
         let dataFile = Bundle.main.url(
           forResource: GameConfiguration.vineDataSpiderFile,
           withExtension: nil),
         let data = try? Data(contentsOf: dataFile),
         let vines = try? decoder.decode([VineDataSpider].self, from: data)
       else {
         return
       }

       for (i, vineData) in vines.enumerated() {
         let anchorPoint = CGPoint(
           x: vineData.relAnchorPoint.x * size.width,
           y: vineData.relAnchorPoint.y * size.height)
         let vine = VineNode(length: vineData.length, anchorPoint: anchorPoint, name: "\(i)")

         vine.addToScene(self)

         vine.attachToPrize(spider)
       }
     }
    
    //Creates a new star field
    private func setUpPhysics() {
      physicsWorld.contactDelegate = self
      physicsWorld.gravity = CGVector(dx: 0.0, dy: -9.8)
      physicsWorld.speed = 1.0
    }
    
    private func setUpScenery(imgname:String) {
      let background =
          SKSpriteNode(imageNamed:imgname )
      background.color =  UIColor.green
      background.anchorPoint = CGPoint(x: 0, y: 0)
      background.position = CGPoint(x: 0, y: 0)
      background.zPosition = Layer.sun
     background.size = CGSize(width:size.width, height: size.height)
        
      addChild(background)
      
//      let water = SKSpriteNode(imageNamed: ImageName.water)
//      water.anchorPoint = CGPoint(x: 0, y: 0)
//      water.position = CGPoint(x: 0, y: 0)
//      water.zPosition = Layer.foreground
//      water.size = CGSize(width: size.width, height: size.height * 0.2139)
//      addChild(water)
      
     
         
    }
    private func setUpNode(node:SKSpriteNode,position:CGPoint,zposition:CGFloat) {
      node.position = position
        node.size = CGSize(width: size.height * 0.3, height: size.height * 0.3)

      node.zPosition = zposition
      node.physicsBody = SKPhysicsBody(
        texture: SKTexture(imageNamed: ImageName.crocMask),
        size: node.size)
      node.physicsBody?.categoryBitMask = PhysicsCategory.hero
      node.physicsBody?.collisionBitMask = 0
      node.physicsBody?.contactTestBitMask = PhysicsCategory.prize
      node.physicsBody?.isDynamic = false
          
      addChild(node)

    }
    private func animateStillImage(leftImage:String,rightImage:String,node:SKSpriteNode) {
    
      let wait = SKAction.wait(forDuration: 1)
      let imageleft = SKAction.setTexture(SKTexture(imageNamed: leftImage))

      let imageright = SKAction.setTexture(SKTexture(imageNamed:rightImage))
      let sequence = SKAction.sequence([imageleft,wait,imageright])
          
      node.run(.repeatForever(sequence))
    }
    private func moveandanimatewith2image(leftImage:String,rightImage:String,node:SKSpriteNode,toPoint:CGPoint) {
    
      let imageleft = SKAction.setTexture(SKTexture(imageNamed: leftImage))
      let wait = SKAction.wait(forDuration: 1)
        let moveBottomLeft = SKAction.move(to: toPoint, duration:1.0)
        let moveRight = SKAction.moveBy(x: size.width/2, y:0, duration:1.0)

      let imageright = SKAction.setTexture(SKTexture(imageNamed:rightImage))
      let sequence = SKAction.sequence([imageleft,moveBottomLeft,wait,imageright,moveRight,wait])
          
      node.run(.repeatForever(sequence))
    }
   
    func animatespider(leftImage:String,rightImage:String) {
   
     let imageleft = SKAction.setTexture(SKTexture(imageNamed: leftImage))
     let wait = SKAction.wait(forDuration: 1)
       let moveBottomLeft = SKAction.move(to: CGPoint(x: 100,y: 100), duration:1.0)
       let moveRight = SKAction.moveBy(x: size.width/2, y:0, duration:1.0)

     let imageright = SKAction.setTexture(SKTexture(imageNamed:rightImage))
     let sequence = SKAction.sequence([imageleft,moveBottomLeft,wait,imageright,moveRight,wait])
         
     spider.run(.repeatForever(sequence))
   }
    
    func touchDown(atPoint pos : CGPoint) {
     
    }
    
    func touchMoved(toPoint pos : CGPoint) {
      
    }
    
    func touchUp(atPoint pos : CGPoint) {
      
    }
   

    func destroy(ball: SKNode) {
        ball.removeFromParent()
    }
    override func update(_ currentTime: TimeInterval) {
           // Called before each frame is rendered
       }
   func didBegin(_ contact: SKPhysicsContact) {
     

       
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let label = self.label {
//            label.run(SKAction.init(named: "Pulse")!, withKey: "fadeInOut")
//        }
        
       // for t in touches { self.touchDown(atPoint: t.location(in: self)) }
        if let touch = touches.first {
            let location = touch.location(in: self)
            let ball = SKSpriteNode(imageNamed: ImageName.ball)
            ball.name = "ball"

            ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.size.width / 2.0)
            ball.physicsBody?.restitution = 0.4
            ball.physicsBody!.contactTestBitMask = ball.physicsBody!.collisionBitMask

            ball.position = location
                ball.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                                             SKAction.fadeOut(withDuration: 1),
                                                             SKAction.removeFromParent()]))
                addChild(ball)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchMoved(toPoint: t.location(in: self)) }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
  
    
}

