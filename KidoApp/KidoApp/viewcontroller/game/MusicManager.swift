//
//  MusicManager.swift
//  KidoApp
//
//  Created by monika kumari on 26/08/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import AVFoundation
class MusicManager {

    static let shared = MusicManager()

    var audioPlayer = AVAudioPlayer()


    private init() { } // private singleton init


    func setup(resourceName:String,oftype:String) {
         do {
            audioPlayer =  try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: resourceName, ofType: oftype)!))
             audioPlayer.prepareToPlay()
            audioPlayer.numberOfLoops = -1


        } catch {
           print (error)
        }
    }


    func play() {
        audioPlayer.play()
    }

    func stop() {
        audioPlayer.stop()
        audioPlayer.currentTime = 0 // I usually reset the song when I stop it. To pause it create another method and call the pause() method on the audioPlayer.
        audioPlayer.prepareToPlay()
    }
}
