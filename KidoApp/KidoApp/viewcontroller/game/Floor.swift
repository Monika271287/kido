//
//  Floor.swift
//  KidoApp
//
//  Created by monika kumari on 08/09/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit

import Foundation
import SpriteKit
let floorCategory  : UInt32 = 0x1 << 1
let RainFallCategory: UInt32 = 0x1 << 2
let playerCategory : UInt32 = 0x1 << 3


class Floor: SKNode {
    
    override init() {
        super.init()
        
        //        set the size and position of the node
        self.position = CGPoint(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.minY + 10)
        self.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: UIScreen.main.bounds.size.width * 4, height: 20))
        self.physicsBody?.affectedByGravity = false
        
        //        apply a physics body to the node
        self.physicsBody?.isDynamic = false
        
        //        set the bitmask properties
        self.physicsBody?.categoryBitMask = floorCategory
        self.physicsBody?.contactTestBitMask = RainFallCategory
        self.physicsBody?.collisionBitMask = playerCategory
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
