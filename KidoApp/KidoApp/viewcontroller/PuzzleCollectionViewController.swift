//
//  PuzzleCollectionViewController.swift
//  KidoApp
//
//  Created by monika kumari on 24/06/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//
import Cheers
import UIKit
import AVFoundation
extension UIImage {
    func resize(_ width: CGFloat, _ height:CGFloat) -> UIImage? {
        let widthRatio  = width / size.width
        let heightRatio = height / size.height
        let ratio = widthRatio > heightRatio ? heightRatio : widthRatio
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
class PuzzleCollectionViewController: BaseViewController,UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDragDelegate, UICollectionViewDropDelegate,UICollectionViewDelegateFlowLayout {
    var objPlayer: AVAudioPlayer?
    var isPlaying:Bool = false

    var puzzleTittle:String = String()
    var puzzle = [ Puzzle(title: "cat", solvedImages: ["catPuzzle1","catPuzzle2","catPuzzle3","catPuzzle4"])]
    @IBOutlet weak var collectionView: UICollectionView!
    let cheerView = CheerView()


    var index: Int = 0
    var gameTimer: Timer?
//    func setGradient(toView:UIView,withColor:[CGColor]) {
//              let gradientLayer = CAGradientLayer()
//              gradientLayer.frame = toView.bounds
//              gradientLayer.colors = [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor]
//              gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
//               gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
//              toView.layer.insertSublayer(gradientLayer, at: 0)
//          }
    @objc func Tap(_ gesture: UIGestureRecognizer){
        
//        switch(gesture.state) {
//        case .began:
//            guard let selectedIndexPath = imgcollection.indexPathForItem(at: gesture.location(in: imgcollection)) else {
//                return
//            }
//            imgcollection.beginInteractiveMovementForItem(at: selectedIndexPath)
//        case .changed:
//            imgcollection.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
//        case .ended:
//            imgcollection.endInteractiveMovement()
//            doneBtn.isHidden = false
//            longPressedEnabled = true
//            self.imgcollection.reloadData()
//        default:
//            imgcollection.cancelInteractiveMovement()
//        }
    }
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()

      cheerView.frame = view.bounds
    }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        cheerView.config.particle = .confetti(allowedShapes: Particle.ConfettiShape.all)
          view.addSubview(cheerView)
        
         puzzle = [ Puzzle(title: self.puzzleTittle, solvedImages: ["\(self.puzzleTittle)1","\(self.puzzleTittle)2","\(self.puzzleTittle)3","\(self.puzzleTittle)4"])]

        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])
        self.collectionView.backgroundColor = UIColor.clear
        //adding longpress gesture over UICollectionView
       let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.Tap(_:)))
        collectionView.addGestureRecognizer(tapGesture)
        collectionView.dragInteractionEnabled = true
        collectionView.dragDelegate = self
        collectionView.dropDelegate = self
        collectionView.reloadData()
    }

   func playAudioFile(audioName:String) {
       guard let url = Bundle.main.url(forResource: audioName, withExtension: "mp3") else { return }

       do {
           try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
           try AVAudioSession.sharedInstance().setActive(true)

           // For iOS 11
           objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

           // For iOS versions < 11
           objPlayer = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

           guard let aPlayer = objPlayer else { return }
           aPlayer.play()

       } catch let error {
           print(error.localizedDescription)
       }
   }
    
  
   // MARK: - UICollectionViewDataSource protocol

    
     func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let solveimg:[String] = ["\(self.puzzleTittle)1","\(self.puzzleTittle)2","\(self.puzzleTittle)3","\(self.puzzleTittle)4"]
       // let puzzle = [Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg)]

        if index < puzzle.count {
            return puzzle[index].unsolvedImages.count
        } else {
            return 0
        }
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCollectionViewCell
        cell.backgroundColor = UIColor.clear
        let solveimg:[String] = ["\(self.puzzleTittle)1","\(self.puzzleTittle)2","\(self.puzzleTittle)3","\(self.puzzleTittle)4"]
             //  let puzzle = [Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg)]
//        let collectionViewWidth = collectionView.bounds.width
//            var customCollectionWidth: CGFloat!
//
//            customCollectionWidth = collectionViewWidth/2

           
        
        cell.puzzleImage.image = UIImage(named: puzzle[index].unsolvedImages[indexPath.item])
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
                      return UIEdgeInsets(top: 40, left: 10, bottom: 40, right: 10)

       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let collectionViewWidth = collectionView.bounds.width
           var customCollectionWidth: CGFloat!
          
           customCollectionWidth = collectionViewWidth/2 - 10

           return CGSize(width: customCollectionWidth, height: customCollectionWidth)
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0
       }
       func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
              let solveimg:[String] = ["\(self.puzzleTittle)1","\(self.puzzleTittle)2","\(self.puzzleTittle)3","\(self.puzzleTittle)4"]
                    // let puzzle = [Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg)]
              let item = puzzle[index].unsolvedImages[indexPath.item]
              let itemProvider = NSItemProvider(object: item as NSString)
              let dragItem = UIDragItem(itemProvider: itemProvider)
              dragItem.localObject = dragItem
              return [dragItem]
          }
       func collectionView(_ collectionView: UICollectionView, dropSessionDidEnd session: UIDropSession) {
           let solveimg:[String] = ["\(self.puzzleTittle)1","\(self.puzzleTittle)2","\(self.puzzleTittle)3","\(self.puzzleTittle)4"]
                 // let puzzle = [Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg)]
           if puzzle[index].unsolvedImages == puzzle[index].solvedImages {
              // Alert.showSolvedPuzzleAlert(on: self)
            if isPlaying {
                isPlaying = false
                objPlayer?.stop()
            }
            else{
                isPlaying = true
                let audiofilename:String = "clap"

                                                self.playAudioFile(audioName: audiofilename)
                cheerView.start()

                  DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                    self.cheerView.stop()
                  }
//                let msg:String = "Great job"
//                                   self.speakgivenSpeech(speech: msg)
                //Alert.showSolvedPuzzleAlert(on: self)
                
                                 
            }
            
               collectionView.dragInteractionEnabled = false
               if index == puzzle.count - 1 {
                   navigationItem.rightBarButtonItem?.isEnabled = false
               } else {
                   navigationItem.rightBarButtonItem?.isEnabled = true
               }
           }
       }





    

    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if collectionView.hasActiveDrag {
            return UICollectionViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
        }
        return UICollectionViewDropProposal(operation: .forbidden)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        
        var destinationIndexPath: IndexPath
        if let indexPath = coordinator.destinationIndexPath {
            destinationIndexPath = indexPath
        } else {
            let row = collectionView.numberOfItems(inSection: 0)
            destinationIndexPath = IndexPath(item: row - 1, section: 0)
        }
        
        if coordinator.proposal.operation == .move {
            reorderItems(coordinator: coordinator, destinationIndexPath: destinationIndexPath, collectionView: collectionView)
            collectionView.reloadData()
        }
    }
    
    fileprivate func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath:IndexPath, collectionView: UICollectionView) {
        
        if let item = coordinator.items.first,
            let sourceIndexPath = item.sourceIndexPath {
            
            collectionView.performBatchUpdates({
                let solveimg:[String] = ["\(puzzleTittle)1","\(self.puzzleTittle)2","\(self.puzzleTittle)3","\(self.puzzleTittle)4"]
                     // let puzzle = [Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg),Puzzle(title: self.puzzleTittle, solvedImages: solveimg)]
                puzzle[index].unsolvedImages.swapAt(sourceIndexPath.item, destinationIndexPath.item)
                collectionView.reloadItems(at: [sourceIndexPath,destinationIndexPath])
                
            }, completion: nil)
            
            coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
        }
    }
}

