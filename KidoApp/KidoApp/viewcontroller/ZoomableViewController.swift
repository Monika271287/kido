//
//  ZoomableViewController.swift
//  HearAfter
//
//  Created by Apple on 21/09/17.
//  Copyright © 2017 monika kumari. All rights reserved.
//

import UIKit
//import imageV
import VariousViewsEffects
import AVFoundation
import Lottie
import FontAwesome_swift
class ZoomableViewController: BaseViewController {

    var animationView_emoji: AnimationView? = .init(name: animationViewFile.emoji.rawValue)

    @IBOutlet var numberStackVtop: UIStackView!
    @IBOutlet var numberStackVbottom: UIStackView!
    @IBOutlet var heightConstant: NSLayoutConstraint!

    @IBOutlet var numberV: UIStackView!
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var innerV: UIView!
    @IBOutlet weak var imageV: UIImageView!
    var originalRect: CGRect!

    var getImage: UIImage!
    var item:String = String()
    var randomImageName:String = String()
    var count:Int = Int()
    var alphabetsImages:[String] = ["apple","ball","cat","dog","elephant","fish","giraffe","horse","ice-cream","jug","kite","lion","mango","nest","octopus","parrot","queen","rabbit","sun","tortoise","umbrella","vase","watch","x-mas tree","yak","zebra"]
    var numberText:[String] = ["one","two","three","four","five","six","seven","eight","nine","ten"]

    var selecteditem:String = String()
    var isanimating:Bool = false

       var index:Int = Int()
    @IBOutlet var monkeyPan: UIPanGestureRecognizer!
   private var glassbreakPlayer: AVAudioPlayer?
    private var explodePlayer: AVAudioPlayer?
    private var jumpPlayer: AVAudioPlayer?
    private var laughPlayer: AVAudioPlayer?
    private var beepPlayer: AVAudioPlayer?
    private var animalSoundPlayer: AVAudioPlayer?

    
    override func viewWillAppear(_ animated: Bool) {
           
       }
     override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: Notification.Name.NSExtensionHostWillEnterForeground, object: nil)
        }
    @objc  override func applicationWillEnterForeground(_ notification: NSNotification) {
          if isanimating == true{
                  isanimating = true
                   laughPlayer?.play()
                         self.animationView_emoji!.play()

              }
              else{
                  isanimating = false
                  self.animationView_emoji!.stop()
                  laughPlayer?.stop()

              }
        
        
        }
        override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
    
            NotificationCenter.default.addObserver(
            self,
            selector: #selector(applicationWillEnterForeground(_:)),
            name: UIApplication.willEnterForegroundNotification,
            object: nil)

          
          
        }
    var isPlaying:Bool = false

    func spellContent()  {
              switch selecteditem {
        case learnListItems.animals.rawValue:
            if self.isPlaying == false{
                self.isPlaying = true
                self.animalSoundPlayer?.play()
            }
            else{
                self.isPlaying = true
                               self.animalSoundPlayer?.stop()
            }
            
            self.animalSoundPlayer!.play()


            case learnListItems.birds.rawValue:
                     let bird:String = item.lowercased()
                      self.speakgivenSpeech(speech: bird)

            
            case learnListItems.instruments.rawValue:
                      let instrument:String = item.lowercased()
                      self.speakgivenSpeech(speech: instrument)



            case learnListItems.alphabets.rawValue:
              let alphabet:String = item.lowercased()
                             self.speakgivenSpeech(speech: alphabet)


            case learnListItems.numbers.rawValue:
                if count > 1{
                let number:String = item.lowercased() + " " + randomImageName.lowercased() + "s"
                                         self.speakgivenSpeech(speech: number)
                }
                else{
                    let number:String = item.lowercased() + " " + randomImageName.lowercased()
                    self.speakgivenSpeech(speech: number)
                }


        default:
        print("")
        }
    }
    func addvolumeButtontoView(containerV:UIView)  {
        let volumeButton:UIButton = UIButton(type: .custom)
        volumeButton.frame =  CGRect(x: 20 , y: 0, width: 70, height: self.view.frame.height/4)
        volumeButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 130, style: .solid)
        volumeButton.setTitle(String.fontAwesomeIcon(name: .volumeDown), for: .normal)
        volumeButton.setTitleColor(UIColor.red, for: .normal)
        volumeButton.addTarget(self, action: #selector(self.volumeButtonPressed(sender:)), for: .touchUpInside)
        containerV.addSubview(volumeButton)
       
    }
    @objc func volumeButtonPressed(sender:UIButton){
         sender.flash()
       
    self.buttonClickedPlayer?.play()
//        imageV.repeatCount = 5.0
//                 imageV.animation = "swing"
//                 imageV.velocity = 1
//                 imageV.damping = 5.0
//                 imageV.animate()
        spellContent()
    }
    func addBananaImageVwithframe(frame:CGRect){
                                let bananaImageV:UIImageView = UIImageView(frame: frame)
                                  bananaImageV.center = numberStackVtop.center
                                  numberStackVtop.backgroundColor = UIColor.clear
                                  bananaImageV.contentMode = .scaleAspectFit
        bananaImageV.image = UIImage(named: randomImageName)
                                  numberStackVtop.addArrangedSubview(bananaImageV)
    }
    
    
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        randomImageName = alphabetsImages.randomElement() ?? ""

        innerV.backgroundColor = UIColor.clear
        self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])

        self.addAnimationView(withname: "emoji", frame: CGRect(x: self.view.frame.width - 150 , y: 0, width: 150, height: self.view.frame.height/4), onview: self.innerV, animationV: animationView_emoji!)
        
        self.addvolumeButtontoView(containerV: self.innerV)
        
      
        // 2
          // 3
          let tapGesture = UITapGestureRecognizer(
            target: self,
            action: #selector(handleTap)
          )
        let tapGesture_emoji = UITapGestureRecognizer(
          target: self,
          action: #selector(handleTapEmoji)
        )
          // 4
          tapGesture.delegate = self
          imageV.addGestureRecognizer(tapGesture)
        animationView_emoji!.addGestureRecognizer(tapGesture_emoji)

         // tapGesture.require(toFail: monkeyPan)

          let tickleGesture = TickleGestureRecognizer(
            target: self,
            action: #selector(handleTickle)
          )
          tickleGesture.delegate = self
          imageV.addGestureRecognizer(tickleGesture)
       // self.imageV?.bringSubviewToFront(self.animationView_emoji!)

        glassbreakPlayer = self.createPlayer(from: audiofilename.glass.rawValue)
          let animal:String = item.lowercased()
        animalSoundPlayer = self.createPlayer(from: animal)
        jumpPlayer = self.createPlayer(from: audiofilename.jump.rawValue)
        explodePlayer = self.createPlayer(from: audiofilename.explosion.rawValue)
        laughPlayer = self.createPlayer(from: audiofilename.laugh.rawValue)
        beepPlayer = self.createPlayer(from: audiofilename.beep.rawValue)
        switch selecteditem {
                     case learnListItems.animals.rawValue:
                        
                       
                                    imageV.image = getImage
                                    numberV.isHidden = true
            
                         case learnListItems.birds.rawValue:
                                      imageV.image = getImage
                                      numberV.isHidden = true

                         case learnListItems.instruments.rawValue:
                                      imageV.image = getImage
                                      numberV.isHidden = true
            
                         case learnListItems.alphabets.rawValue:
                                        imageV.image = UIImage(named:self.alphabetsImages[index])!
                                        numberV.isHidden = true
                        
                               
                         case learnListItems.numbers.rawValue:
                            numberV.backgroundColor = UIColor.clear
                            imageV.isHidden = true
                            numberV.isHidden = false
                            numberLabel.textColor = UIColor.white
                            numberLabel.font = Utility.fontWithSize(size: (100 * Utility.getScreenScaleFactor()))
                           
                            count = Int(item) ?? 0
                           if count == 1 {
                            numberStackVbottom.removeFromSuperview()
                             let newMultiplier:CGFloat = 1.0
                                               heightConstant = heightConstant.setMultiplier(multiplier: newMultiplier)
                            self.addBananaImageVwithframe(frame: CGRect(x: 0, y: 0, width: numberStackVtop.frame.size.width, height:       numberStackVtop.frame.size.height))
                                                          }
                           else{
                            let midindex:Int = count/2
                            for index in 1...count{
                               
                            
                                if index <=  midindex{

                              addBananaImageVwithframe(frame: CGRect(x: 0, y: 0, width: numberStackVtop.frame.size.width, height:       numberStackVtop.frame.size.height/2))
                                }
                                else{
                                            
                                                                  let bananaImageV:UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: numberStackVtop.frame.size.width, height: numberStackVtop.frame.size.height/2))
                                                              bananaImageV.center = numberStackVtop.center
                                                              numberStackVtop.backgroundColor = UIColor.clear
                                                              bananaImageV.contentMode = .scaleAspectFit
                                                              
                                                              bananaImageV.image = UIImage(named: randomImageName)

                                                              numberStackVbottom.addArrangedSubview(bananaImageV)
                                                          }
                                
                            }
                            }
                            
                            
                          
            numberLabel.text = item

                     default:
                  imageV.image = UIImage(named:"")!

                     }
        
        spellContent()
        // Do any additional setup after loading the view.
    }
    
        /*@IBAction func handlePan(_ gesture: UIPanGestureRecognizer) {
          // 1
          let translation = gesture.translation(in: view)

          // 2
          guard let gestureView = gesture.view else {
            return
          }

          gestureView.center = CGPoint(
            x: gestureView.center.x + translation.x,
            y: gestureView.center.y + translation.y
          )

          // 3
          gesture.setTranslation(.zero, in: view)

          guard gesture.state == .ended else {
            return
          }

          // 4
          let velocity = gesture.velocity(in: view)
          let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
          let slideMultiplier = magnitude / 200

          // 5
          let slideFactor = 0.1 * slideMultiplier
          // 6
          var finalPoint = CGPoint(
            x: gestureView.center.x + (velocity.x * slideFactor),
            y: gestureView.center.y + (velocity.y * slideFactor)
          )

          // 7
          finalPoint.x = min(max(finalPoint.x, 0), view.bounds.width)
          finalPoint.y = min(max(finalPoint.y, 0), view.bounds.height)

          // 8
          UIView.animate(
            withDuration: Double(slideFactor * 2),
            delay: 0,
            // 9
            options: .curveEaseOut,
            animations: {
              gestureView.center = finalPoint
          })
        }*/
          
        @IBAction func handlePinch(_ gesture: UIPinchGestureRecognizer) {
          guard let gestureView = gesture.view else {
            return
          }

          gestureView.transform = gestureView.transform.scaledBy(
            x: gesture.scale,
            y: gesture.scale
          )
          gesture.scale = 1
        }
        
        @IBAction func handleRotate(_ gesture: UIRotationGestureRecognizer) {
          guard let gestureView = gesture.view else {
            return
          }

          gestureView.transform = gestureView.transform.rotated(
            by: gesture.rotation
          )
          gesture.rotation = 0
        }
        func addjumpbehaviour()  {
                   originalRect = imageV.frame
                           imageV.frame = originalRect
                      self.addGravity(toImageV: imageV)
                      self.addBounce(toImageV: imageV)
                  }
    @objc func handleTapEmoji(_ gesture: UITapGestureRecognizer) {
        if isanimating == false{
            isanimating = true
//            beepPlayer?.play()
             laughPlayer?.play()
            
                   self.animationView_emoji!.play()

        }
        else{
            isanimating = false
            self.animationView_emoji!.stop()
//            beepPlayer?.stop()
            laughPlayer?.stop()

        }
             

           }

        @objc func handleTap(_ gesture: UITapGestureRecognizer) {
            originalRect = imageV.frame
            jumpPlayer?.play()
            let _:CGFloat = self.imageV.frame.origin.y
            
            UIView.animate(withDuration: 1, delay: 0.01, usingSpringWithDamping: 0.5, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.imageV.frame.origin.y -= 200
            }) { _ in
                                self.imageV.frame.origin.y += 200

            }
        }

        @objc func handleTickle(_ gesture: TickleGestureRecognizer) {
       //   laughPlayer?.play()
//            imageV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
//                self.laughPlayer?.stop()
//                            self.reshowImage()
//                        })
        }
//        @IBOutlet weak var exampleView: UIView?

         @IBAction func explode(_ sender: UIButton) {
              sender.flash()
    self.buttonClickedPlayer?.play()
             explodePlayer?.play()
            switch selecteditem {
            case learnListItems.animals.rawValue:
                    imageV?.explode(completion: {
                                   self.reshowImage()
                                  self.explodePlayer?.stop()
                               })

                case learnListItems.birds.rawValue:
                                  imageV?.explode(completion: {
                                                 self.reshowImage()
                                                self.explodePlayer?.stop()
                                             })

                case learnListItems.instruments.rawValue:
                                  imageV?.explode(completion: {
                                                 self.reshowImage()
                                                self.explodePlayer?.stop()
                                             })
                case learnListItems.alphabets.rawValue:
                       imageV?.explode(completion: {
                                                                    self.reshowImage()
                                                                   self.explodePlayer?.stop()
                                                                })
                      
                case learnListItems.numbers.rawValue:
                  
                   numberV?.explode(completion: {
                                                      self.reshowView()
                                                     self.explodePlayer?.stop()
                                                  })
                   
                   
                 

            default:
              numberV?.explode(completion: {
                                                self.reshowView()
                                               self.explodePlayer?.stop()
                                            })

            }
            
         }

         @IBAction func breakGlass(_ sender: UIButton) {
              sender.flash()
    self.buttonClickedPlayer?.play()
            glassbreakPlayer?.play()
                    switch selecteditem {
                                 case learnListItems.animals.rawValue:
                                        imageV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
                                                 self.reshowImage()
                                                 self.glassbreakPlayer?.stop()
                                             })

                                     case learnListItems.birds.rawValue:
                                                      imageV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
                                                               self.reshowImage()
                                                               self.glassbreakPlayer?.stop()
                                                           })

                                     case learnListItems.instruments.rawValue:
                                                      imageV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
                                                               self.reshowImage()
                                                               self.glassbreakPlayer?.stop()
                                                           })
                                     case learnListItems.alphabets.rawValue:
                                           imageV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
                                                                                                      self.reshowImage()
                                                                                                      self.glassbreakPlayer?.stop()
                                                                                                  })
                                           
                                     case learnListItems.numbers.rawValue:
                                       
                                        numberV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
                                                     self.reshowView()
                                                     self.glassbreakPlayer?.stop()
                                                 })
                                        
                                        
                                      

                                 default:
                                  imageV?.breakGlass(size: GridSize(columns: 15, rows: 21), completion: {
                                                                                         self.reshowView()
                                                                                         self.glassbreakPlayer?.stop()
                                                                                     })

                                 }
         
         }

         @IBAction func showSnowflakes(_ sender: UIButton) {
              sender.flash()
    self.buttonClickedPlayer?.play()
            switch selecteditem {
            case learnListItems.animals.rawValue:
                                 imageV?.addSnowflakes(amount: 10, speed: .slow)


                case learnListItems.birds.rawValue:
                                              imageV?.addSnowflakes(amount: 10, speed: .slow)


                case learnListItems.instruments.rawValue:
                                               imageV?.addSnowflakes(amount: 10, speed: .slow)

                case learnListItems.alphabets.rawValue:
                                  imageV?.addSnowflakes(amount: 10, speed: .slow)

                      
                case learnListItems.numbers.rawValue:
                  
                                numberV?.addSnowflakes(amount: 10, speed: .slow)

                   
                   
                 

            default:
                          imageV?.addSnowflakes(amount: 10, speed: .slow)


            }
         }

    private func reshowView() {
        self.numberV?.alpha = 0
        self.numberV?.isHidden = false

        UIView.animate(withDuration: 1, animations: {
            self.numberV?.alpha = 1
        })
    }
         private func reshowImage() {
             self.imageV?.alpha = 0
             self.imageV?.isHidden = false

             UIView.animate(withDuration: 1, animations: {
                 self.imageV?.alpha = 1
             })
         }
       
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ZoomableViewController: UIGestureRecognizerDelegate {
  func gestureRecognizer(
    _ gestureRecognizer: UIGestureRecognizer,
    shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer
  ) -> Bool {
    return true
  }
}
