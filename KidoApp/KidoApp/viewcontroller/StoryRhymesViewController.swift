//
//  StoryRhymesViewController.swift
//  KidoApp
//
//  Created by monika kumari on 27/07/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import Lottie
import AVFoundation
import SpriteKit
import GameplayKit
import Speech
import MarqueeLabel

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}
class StoryRhymesViewController: BaseViewController,AVSpeechSynthesizerDelegate {
    let kalertTagJustDismissAlert = 103
    var waterController: WaterController!

    var animationView_mary: AnimationView? = .init(name: animationViewFile.plant.rawValue)
    var animationView_boat: AnimationView? = .init(name:animationViewFile.boat.rawValue)
    var animationView_catchfish: AnimationView? = .init(name: animationViewFile.fish.rawValue)


    @IBOutlet var innerV: SKView!
    var selectedItems = String()
    private var flipsoundPlayer: AVAudioPlayer?

    @IBOutlet var pauseButton: UIButton!
    
    private var isFlipped: Bool = false

    @IBOutlet var lblText1: MarqueeLabel!
    @IBOutlet var lblText2: MarqueeLabel!
    @IBOutlet var containerV1: UIView!
    @IBOutlet var containerV2: UIView!
    var page:Int = 0
//    var speechSynthesizer = AVSpeechSynthesizer()
    @IBOutlet var prevButton: UIButton!
       @IBOutlet var nextButton: UIButton!
   var lineindex = 0
    var isSpeaking:Bool = true
    var volumeBtn:UIBarButtonItem = UIBarButtonItem()
    
    var selectedIndex = Int()
    var isplaying:Bool = false
    var storyArray1 = [""]
        
    var soundfilesArray = [SoundFile.baba,SoundFile.bingo,SoundFile.insy,SoundFile.rowyourboat]

    var RhymesArray1 = ["Baa baa black sheep, have you any wool?  Yes sir, yes sir,three one for the dame, And one for the little boy who lives down the lane.",
                        
" Twinkle, twinkle, little star,How I wonder what you are.Up above the world so high,Like a diamond `in the sky.",

"Incy Wincy spider climbing up the spout.Down came the rain, and washed poor Incy out.Up came the sun, and dried up all the rainAnd Incy Wincy spider went climbing up again.",

"Row, row, row your boat.Gently down the stream.Merrily merrily, merrily, merrily.Life is but a dream."]
    


       override func viewDidDisappear(_ animated: Bool) {
      NotificationCenter.default.removeObserver(self, name: Notification.Name.NSExtensionHostWillEnterForeground, object: nil)
        self.stopaudio()
//        if self.title == RhymeTitle.Rhyme5{
//            self.waterController.stop()
//        }
        
      }
    func gettextfromaudio(songfilename:String,extensionName:String)  {
        if let audioURL = Bundle.main.url(forResource: songfilename, withExtension: extensionName) {
            do {
                let recognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-UK"))
                let request = SFSpeechURLRecognitionRequest(url: audioURL)

                request.shouldReportPartialResults = true

                if (recognizer?.isAvailable)! {

                    recognizer?.recognitionTask(with: request) { result, error in
                        guard error == nil else { print("Error: \(error!)"); return }
                        guard let result = result else { print("No result!"); return }

                        print(result.bestTranscription.formattedString)
//                        self.lblText1.text = result.bestTranscription.formattedString
                        
                    }
                } else {
                    print("Device doesn't support speech recognition")
                }
//                let myData = try Data(contentsOf: audioURL)
//                print(myData.count)
            } catch {
                print(error)
            }
        }
//       let audioURL = Bundle.main.url(forResource: songfilename, withExtension: extensionName)
        
    }
    @objc override func applicationWillEnterForeground(_ notification: NSNotification) {
       
//        if self.title == StoryTitle.story1{
//            self.addSwingInfinityAnimationTo(imageV:self.imgV)
//
//        }
//        if self.title == StoryTitle.story2{
//                  self.addSwingInfinityAnimationTo(imageV:self.imgV)
//
//              }
//        if self.title == RhymeTitle.Rhyme1{
//
//                                self.addSwingInfinityAnimationTo(imageV:self.imgV)
//
//        }
//        if tag == Imagetag.swinganimation.rawValue  ||  tag == Imagetag.swinganimation.rawValue ||  tag == Imagetag.waterspayer.rawValue {

    
    }
    func setupMarquee(marqueeLabel:MarqueeLabel)  {
        marqueeLabel.tag = 301
               marqueeLabel.type = .leftRight
               marqueeLabel.speed = .rate(20)
               marqueeLabel.fadeLength = 10.0
               marqueeLabel.leadingBuffer = 30.0
               marqueeLabel.trailingBuffer = 20.0
               marqueeLabel.textAlignment = .center
        marqueeLabel.textColor = UIColor.white
               
    }
    override func viewDidAppear(_ animated: Bool) {
             super.viewDidAppear(animated)
       
               NotificationCenter.default.addObserver(
               self,
               selector: #selector(applicationWillEnterForeground(_:)),
               name: UIApplication.willEnterForegroundNotification,
               object: nil)

             
             
           }
    var xpading:CGFloat = (ScreenSize.SCREEN_WIDTH/4)
    var sunYpading:CGFloat = (ScreenSize.SCREEN_HEIGHT/2)/8
    var sun_width:CGFloat = ScreenSize.SCREEN_WIDTH/8
    var sun_heighr:CGFloat = ScreenSize.SCREEN_WIDTH/4
    func updatescene(index:Int)  {
        if isplaying == true{
                  stopMusic()
               }
        scene.removeFromParent()
      
        scene = StoryRhymeScene(size: CGSize(width: innerV.frame.width, height: innerV.frame.height))
        scene.scaleMode = .aspectFill

        switch index {
                  case 0:
                       scene.rhymename = RhymeListType.babablacksheep
                      case 1:
                                  scene.rhymename = RhymeListType.bingo
                      case 2:
                                  scene.rhymename = RhymeListType.insywinsyspider
                      case 3:
                                  scene.rhymename = RhymeListType.rowyourboat
                  default:
                      dLog(message: "")
                  }
                 
                      innerV.presentScene(scene)
    
        self.title = RhymeTitle.Rhyme1
        lblText1.text = RhymesArray1[lineindex]

        self.playMusic()
    }
    var scene:StoryRhymeScene = StoryRhymeScene()
    func createUIForRhyme1()  {

                      innerV.showsFPS = false
                      innerV.showsNodeCount = false
                      innerV.ignoresSiblingOrder = false
                      // Create and configure the scene.
                     scene = StoryRhymeScene(size: CGSize(width: innerV.frame.width, height: innerV.frame.height))
        scene.scaleMode = .aspectFill
                      innerV.presentScene(scene)
        
        self.title = RhymeTitle.Rhyme1
        lblText1.text = RhymesArray1[lineindex]

        self.playMusic()
       }
    
    func createUIForRhyme2()  {
            self.addwaveanimation(imageName: rhymesstorybackgroundImage.ship.rawValue, innerV_: innerV, frame_: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH/2, height: ScreenSize.SCREEN_WIDTH/2), tag: Imagetag.ship.rawValue)
        self.title = RhymeTitle.Rhyme1
                                         lblText1.text = RhymesArray1[lineindex]
       }
    func createUIForRhyme3()  {
        self.addwaveanimation(imageName: rhymesstorybackgroundImage.fish.rawValue, innerV_: innerV, frame_: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH/2, height: ScreenSize.SCREEN_WIDTH/2), tag: Imagetag.ship.rawValue)
                       lblText1.text = RhymesArray1[lineindex]
         self.addwaveanimation(imageName: rhymesstorybackgroundImage.fish.rawValue, innerV_: innerV, frame_: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH/2, height: ScreenSize.SCREEN_WIDTH/2), tag: Imagetag.fish.rawValue)
//        self.addAnimationView(withname: animationViewFile.fish.rawValue, frame: CGRect(x: self.view.frame.width - 150 , y: 0, width: 150, height: self.view.frame.height/4), onview: self.innerV, animationV: animationView_catchfish!)

                       self.title = RhymeTitle.Rhyme3
                                      lblText1.text = RhymesArray1[lineindex]
    }
//    func createUIForRhyme4()  {
//        self.addImageToInnerV(imageName: rhymesstorybackgroundImage.Rhymes4papa.rawValue, containerV: innerV, frame_: CGRect(x:0 , y: 80, width: ((ScreenSize.SCREEN_WIDTH/2)), height: ((ScreenSize.SCREEN_HEIGHT/2 ) - 50.0)),tag: Imagetag.papa.rawValue)
//
//        self.addImageToInnerV(imageName: rhymesstorybackgroundImage.Rhymes4sugar.rawValue, containerV: innerV, frame_: CGRect(x:(ScreenSize.SCREEN_WIDTH/2) , y: ScreenSize.SCREEN_HEIGHT/2, width: ((ScreenSize.SCREEN_WIDTH/8)), height: ((ScreenSize.SCREEN_WIDTH/4))),tag: Imagetag.sugar.rawValue)
//
//        self.addImageToInnerV(imageName: rhymesstorybackgroundImage.Rhymes4Johny.rawValue, containerV: innerV, frame_: CGRect(x:(ScreenSize.SCREEN_WIDTH/2) , y: 80, width: ((ScreenSize.SCREEN_WIDTH/2)), height: ((ScreenSize.SCREEN_HEIGHT/2 ) - 50.0)),tag: Imagetag.swinganimation.rawValue)
//
//                           self.title = RhymeTitle.Rhyme4
//                                          lblText1.text = RhymesArray1[lineindex]
//        }
//    func createUIForRhyme5()  {
//
//            waterController = WaterController(view: self.innerV)
//                   waterController.start()
//        self.addImageToInnerV(imageName: rhymesstorybackgroundImage.Rhymes5Boy.rawValue, containerV: innerV, frame_: CGRect(x: (ScreenSize.SCREEN_WIDTH/2)-40 , y: 60, width: ((ScreenSize.SCREEN_WIDTH/2)), height: ((ScreenSize.SCREEN_HEIGHT/2 ) - 50.0)),tag: Imagetag.swinganimation.rawValue)
//
////                           self.title = RhymeTitle.Rhyme5
//                                          lblText1.text = RhymesArray1[lineindex]
//        }
//    func createUIForStory1()  {
//
//        self.innerV.backgroundColor = UIColor(patternImage: UIImage(named: rhymesstorybackgroundImage.story1BG.rawValue)!)
//
//                                            let height_:CGFloat = ScreenSize.SCREEN_WIDTH/3
//                                            let width_:CGFloat = ScreenSize.SCREEN_WIDTH/3
//                                            let ypadding = ((ScreenSize.SCREEN_HEIGHT - height_)/2)-xpading
//                                            let xcor_crow:CGFloat = ((ScreenSize.SCREEN_WIDTH - width_)/2) - (ScreenSize.SCREEN_WIDTH/4) + width_
//
//                                            self.addImageToInnerV(imageName: rhymesstorybackgroundImage.story1crow.rawValue, containerV: innerV, frame_: CGRect(x: ((ScreenSize.SCREEN_WIDTH - width_)/2) - (ScreenSize.SCREEN_WIDTH/4), y: ypadding , width: width_, height: height_ ),tag: Imagetag.crow.rawValue)
//
//
//                                            self.addImageToInnerV(imageName: rhymesstorybackgroundImage.story1pot.rawValue, containerV: innerV, frame_: CGRect(x: xcor_crow + 10 , y: ypadding, width: width_, height: height_), tag: Imagetag.pot.rawValue)
//
//                                            self.addImageToInnerV(imageName: rhymesstorybackgroundImage.sun.rawValue, containerV: innerV, frame_: CGRect(x: sun_width + xpading , y: sunYpading, width: sun_width, height: sun_heighr), tag: Imagetag.swinganimation.rawValue)
//
//        self.title = StoryTitle.story1
//                                                                    lblText1.text = storyArray1[lineindex]
//                 }
//    func createUIForStory2()  {
//          self.title = StoryTitle.story2
//        self.assignbackground(backgroundimage: UIImage(named: rhymesstorybackgroundImage.story2BG.rawValue)!, toview: self.innerV, frame_:CGRect(x: 0 , y: 80.0 , width: ScreenSize.SCREEN_WIDTH, height:ScreenSize.SCREEN_HEIGHT/2 ) ,tag:Imagetag.FlashAnimation.rawValue)
//                              let height_:CGFloat = ScreenSize.SCREEN_WIDTH/3
//                              let width_:CGFloat = ScreenSize.SCREEN_WIDTH/3
//                              let ypadding = ((ScreenSize.SCREEN_HEIGHT)/2 + 60)-height_
//                              let xcor_crow:CGFloat = ((ScreenSize.SCREEN_WIDTH - width_)/2) - (ScreenSize.SCREEN_WIDTH/4) + width_
//
//
//                              self.addImageToInnerV(imageName: rhymesstorybackgroundImage.story2lion.rawValue, containerV: innerV, frame_: CGRect(x: ((ScreenSize.SCREEN_WIDTH - width_)/2) - (ScreenSize.SCREEN_WIDTH/4), y: ypadding , width: width_, height: height_ ), tag: Imagetag.lion.rawValue)
//
//
//                              self.addImageToInnerV(imageName: rhymesstorybackgroundImage.story2Rabbit.rawValue, containerV: innerV, frame_: CGRect(x: xcor_crow + 10 , y: ypadding, width: width_, height: height_), tag: Imagetag.swinganimation.rawValue)
//
//
//
//                                                      lblText1.text = storyArray1[lineindex]
//    }
    override func viewWillAppear(_ animated: Bool) {
                    self.gettextfromaudio(songfilename: SoundFile.baba, extensionName: "mp3")

    }
    func setCustomButtons()  {
                self.setupfontAwesomeIconToButton(button: self.pauseButton, iconname: .pause, fontsize: 30, color: .red)
                self.setupfontAwesomeIconToButton(button: self.nextButton, iconname: .forward, fontsize: 30, color: .red)
                self.setupfontAwesomeIconToButton(button: self.prevButton, iconname: .backward, fontsize: 30, color: .red)

                pauseButton.addTarget(self, action: #selector(self.pauseClicked(_:)), for: .touchUpInside)
                nextButton.addTarget(self, action: #selector(self.nextClicked(_:)), for: .touchUpInside)
                prevButton.addTarget(self, action: #selector(self.prevClicked(_:)), for: .touchUpInside)
    }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        self.setCustomButtons()

        speechSynthesizer.delegate = self
        
        self.view.backgroundColor = UIColor.darkGray

        flipsoundPlayer = self.createPlayer(from: audiofilename.flip.rawValue)

        self.setUpNavigationBar()
     
        switch selectedItems {
        case learnListItems.rhymes.rawValue:
            lblText1.font = Utility.fontWithSize(size: (20 * Utility.getScreenScaleFactor()))
            lblText2.font = Utility.fontWithSize(size: (20 * Utility.getScreenScaleFactor()))
            self.setupMarquee(marqueeLabel: lblText1)
            self.setupMarquee(marqueeLabel: lblText2)

            self.createUIForRhyme1()

            case learnListItems.story.rawValue:
                lblText1.font = Utility.fontWithSize(size: (18 * Utility.getScreenScaleFactor()))
                lblText2.font = Utility.fontWithSize(size: (18 * Utility.getScreenScaleFactor()))

                       if selectedIndex == 0{
//                        self.createUIForStory1()
//                        self.spellContent()


                       }
                       else{
//                        self.createUIForStory2()
//                        self.spellContent()



                       }
        default:
                            self.title = ""

        }
        
    }
    
    func refreshArrayAndUpdateUI()  {
        switch selectedItems {
        case learnListItems.rhymes.rawValue:
                isFlipped = !isFlipped

                       let cardToFlip = isFlipped ? containerV1 : containerV2
                       let bottomCard = isFlipped ? containerV2 : containerV1
                flipsoundPlayer?.play()
                
              
                       UIView.transition(from: cardToFlip!,
                                         to: bottomCard!,
                                         duration: 0.5,
                                         options: [.transitionCurlUp, .showHideTransitionViews],
                                         completion: {done in
                                            if self.isFlipped == true{
                                                                                       self.lblText2.text = self.RhymesArray1[self.lineindex]

                                                                                   }
                                                                                   else{
                                                                                       self.lblText1.text = self.RhymesArray1[self.lineindex]
                                                                                 
                                                                                   }
                                            self.updatescene(index: self.lineindex)

                                            
                                       

                       })

            
            case learnListItems.story.rawValue:
                     if selectedIndex == 0{
                         isFlipped = !isFlipped

                                let cardToFlip = isFlipped ? containerV1 : containerV2
                                let bottomCard = isFlipped ? containerV2 : containerV1
                        flipsoundPlayer?.play()

                                UIView.transition(from: cardToFlip!,
                                                  to: bottomCard!,
                                                  duration: 0.5,
                                                  options: [.transitionCurlUp, .showHideTransitionViews],
                                                  completion: {done in
                                                     if self.isFlipped == true{
                                                         self.lblText2.text = self.storyArray1[self.lineindex]
                                                        self.spellContent()

                                                     }
                                                     else{
                                                         self.lblText1.text = self.storyArray1[self.lineindex]
                                                        self.spellContent()


                                                     }


                                })

                     }
                     else{
                         isFlipped = !isFlipped

                                let cardToFlip = isFlipped ? containerV1 : containerV2
                                let bottomCard = isFlipped ? containerV2 : containerV1
                        flipsoundPlayer?.play()

                                UIView.transition(from: cardToFlip!,
                                                  to: bottomCard!,
                                                  duration: 0.5,
                                                  options: [.transitionCurlUp, .showHideTransitionViews],
                                                  completion: {done in
                                                     if self.isFlipped == true{
                                                         self.lblText2.text = self.storyArray1[self.lineindex]
                                                        self.spellContent()

                                                     }
                                                     else{
                                                         self.lblText1.text = self.storyArray1[self.lineindex]
                                                        self.spellContent()


                                                     }


                                })

                     }

        default:
             lblText1.text = RhymesArray1[lineindex]
        }
       
        
    }
    
    func turnThePage()  {
        
       
         switch selectedItems {
               case learnListItems.rhymes.rawValue:
                       if (lineindex + 1) < RhymesArray1.count {
                                                  lineindex = lineindex + 1
                                                                     playMusic()
                                                  refreshArrayAndUpdateUI()

                                              }
                                              else{
                                                  lineindex = 0
                                                  refreshArrayAndUpdateUI()

                                              }
                   case learnListItems.story.rawValue:
                                 if selectedIndex == 0{
                                         if (lineindex + 1) < storyArray1.count {
                                                     lineindex = lineindex + 1
                                                     
                                                     refreshArrayAndUpdateUI()

                                                 }
                                                 else{
                                                     lineindex = 0
                                                     refreshArrayAndUpdateUI()

                                                 }

                                     }
                                     else{
                                          if (lineindex + 1) < storyArray1.count {
                                                     lineindex = lineindex + 1
                                                     
                                                     refreshArrayAndUpdateUI()

                                                 }
                                                 else{
                                                     lineindex = 0
                                                     refreshArrayAndUpdateUI()

                                                 }

                                     }
               default:
                                   self.title = ""

               }

    }
    func stopMusic()  {
        if self.isFlipped == true{
            lblText2.pauseLabel()
        }
        else{
            lblText1.pauseLabel()
        }
             isplaying = false
                      MusicManager.shared.stop()
                      pauseButton.setTitle(String.fontAwesomeIcon(name: .play), for: .normal)
       }
    func playMusic()  {
        if self.isFlipped == true{
            lblText2.unpauseLabel()
        }
        else{
            lblText1.unpauseLabel()
        }
        if lineindex < soundfilesArray.count{
        MusicManager.shared.setup(resourceName: soundfilesArray[lineindex], oftype: "mp3")
         MusicManager.shared.play()
                   pauseButton.setTitle(String.fontAwesomeIcon(name: .stop), for: .normal)
                   isplaying = true
            
        }
    }
    @objc func pauseClicked(_ sender: UIButton) {
            sender.flash()
        self.buttonClickedPlayer?.play()
        
        if isplaying == false{
           playMusic()
        }
        else{
          stopMusic()

        }

           
       }
     @objc func nextClicked(_ sender: UIButton) {
          sender.flash()
    self.buttonClickedPlayer?.play()
        self.turnThePage()

         
     }
     @objc func prevClicked(_ sender: UIButton) {
          sender.flash()
    self.buttonClickedPlayer?.play()
         if (lineindex - 1) > 0 {

         lineindex = lineindex - 1
          refreshArrayAndUpdateUI()
         }
         else{
             lineindex = 0
               refreshArrayAndUpdateUI()
         }


         
     }
          
//           func setupNextButtontoView()  {
//                         nextButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 50, style: .solid)
//                           nextButton.setTitle(String.fontAwesomeIcon(name: .stepForward), for: .normal)
//                           nextButton.setTitleColor(UIColor.black, for: .normal)
//                           nextButton.addTarget(self, action: #selector(self.nextClicked(_:)), for: .touchUpInside)
//                       }
//              func setupPrevButtontoView()  {
//                      prevButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 50, style: .solid)
//                        prevButton.setTitle(String.fontAwesomeIcon(name: .stepBackward), for: .normal)
//                        prevButton.setTitleColor(UIColor.black, for: .normal)
//                        prevButton.addTarget(self, action: #selector(self.prevClicked(_:)), for: .touchUpInside)
//                    }
//
    
           func spellContent()  {
            if isFlipped == true{
                               guard let string = lblText2.text else { return  }
                                          self.speakgivenSpeech(speech: string)
                           }
                           else{
                               guard let string = lblText1.text else { return  }
                                          self.speakgivenSpeech(speech: string)
                           }
          
                    
                 
            }
    func stopaudio()  {
        let image = UIImage.fontAwesomeIcon(name: .volumeOff, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
         volumeBtn.image = image
         volumeBtn.tintColor = UIColor.blue
         self.isSpeaking = false
                   self.stopSpeech()
    }
        @objc func stopaudio(_ sender: UIBarButtonItem) {
           if isSpeaking == true{
                                             self.stopaudio()

                             }
                             else{
                                 let image = UIImage.fontAwesomeIcon(name: .volumeUp, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
                                 volumeBtn.tintColor = UIColor.red
                                 volumeBtn.image = image
                                 self.isSpeaking = true
                                           self.spellContent()
                             }
          
            
        }
    @objc func refreshClicked(){
        
    }
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        MusicManager.shared.stop()


    }
    

        func setUpNavigationBar()  {
                   self.navigationController?.navigationBar.isHidden=false
                     let image = UIImage.fontAwesomeIcon(name: .volumeUp, style: .solid, textColor: UIColor.black, size: CGSize(width: 40, height: 40))
                      volumeBtn = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(self.stopaudio(_:)))
            volumeBtn.tintColor  = UIColor.red
                     self.navigationItem.rightBarButtonItem  = volumeBtn
           
                     
              }
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, willSpeakRangeOfSpeechString characterRange: NSRange, utterance: AVSpeechUtterance) {
           let mutableAttributedString = NSMutableAttributedString(string: utterance.speechString)
           mutableAttributedString.addAttribute(.foregroundColor, value: UIColor.red, range: characterRange)
        if isFlipped == true{
            lblText2.attributedText = mutableAttributedString

        }
        else{
            lblText1.attributedText = mutableAttributedString

        }
        
       }
       
       func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        if utterance.speechString != "Thanks."{
            
        self.turnThePage()
            if self.title == RhymeTitle.Rhyme2{
                       self.addwaveanimation(imageName: rhymesstorybackgroundImage.ship.rawValue, innerV_: innerV, frame_: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH/2, height: ScreenSize.SCREEN_WIDTH/2), tag: Imagetag.ship.rawValue)

                       
                   }
        }
        else{
            if self.title == RhymeTitle.Rhyme2{
                       self.addwaveanimation(imageName: rhymesstorybackgroundImage.ship.rawValue, innerV_: innerV, frame_: CGRect(x: 0, y: 0, width: ScreenSize.SCREEN_WIDTH/2, height: ScreenSize.SCREEN_WIDTH/2), tag: Imagetag.ship.rawValue)

                       
                   }
            self.ShowAlertViewWithAction(message: "DO YOU WANT TO START AGAIN", buttonCount: 2, ONVC: self, AlertTag: 1, isSuccess: true)
         /* Utility.showAlertControllerWith(title: "THE END", message: "DO YOU WANT TO START AGAIN", onVc: self, style: UIAlertController.Style.actionSheet, buttons: ["YES","NO"]) { (succes, index)  in
                switch index {
                case 0:
                    self.turnThePage()
                    
                    break // ok tapped

                    
                case 2:break // custom tapped
                    
                default:
                    break
                }
            }*/
        }
        if isFlipped == true{

                  lblText2.attributedText = NSAttributedString(string: utterance.speechString)
            

              }
              else{

                  lblText1.attributedText = NSAttributedString(string: utterance.speechString)

              }
     
              
       }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK- AlertviewControllerDelegates

    override func YESAction(_ sender: UIButton){
        self.turnThePage()

    }
    
    
    override func NOAction(_ sender: UIButton){
        
    }

}
