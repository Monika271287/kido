//
//  BaseViewController.swift
//  KidoApp
//
//  Created by monika kumari on 29/05/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import AVFoundation

@IBDesignable class RoundedImage: UIImageView
{
    override func layoutSubviews() {
        super.layoutSubviews()

        updateCornerRadius()
    }

    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            updateCornerRadius()
        }
    }

    func updateCornerRadius() {
        layer.cornerRadius = rounded ? cornerRadius : 0
        layer.masksToBounds = rounded ? true : false
    }
}

@IBDesignable class RoundedView: UIView
{
    override func layoutSubviews() {
        super.layoutSubviews()

        updateCornerRadius()
    }

    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            updateCornerRadius()
        }
    }

    func updateCornerRadius() {
        layer.cornerRadius = rounded ? cornerRadius : 0
        layer.masksToBounds = rounded ? true : false
    }
}

@IBDesignable class RoundedButton: UIButton
{
    override func layoutSubviews() {
        super.layoutSubviews()

        updateCornerRadius()
    }

    @IBInspectable var rounded: Bool = false {
        didSet {
            updateCornerRadius()
        }
    }

    @IBInspectable var cornerRadius: CGFloat = 0.1 {
        didSet {
            updateCornerRadius()
        }
    }

    func updateCornerRadius() {
        layer.cornerRadius = rounded ? cornerRadius : 0
        layer.masksToBounds = rounded ? true : false
    }
}
extension NSLayoutConstraint {
    /**
     Change multiplier constraint

     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
    */
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {

        NSLayoutConstraint.deactivate([self])

        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}

class BaseViewController: UIViewController {
    func customizeGivenButtonwithText(ginvenbutton:UIButton,text:String,fontsize:CGFloat,backgrouncolor:UIColor,textcolor:UIColor)  {
     ginvenbutton.titleLabel?.font = Utility.fontWithSize(size: (fontsize * Utility.getScreenScaleFactor()))
        ginvenbutton.layer.cornerRadius = 10
        ginvenbutton.layer.borderWidth = 2
        ginvenbutton.layer.borderColor = UIColor.black.cgColor
        ginvenbutton.backgroundColor = backgrouncolor
        ginvenbutton.titleLabel?.textColor = textcolor
        ginvenbutton.titleLabel?.textAlignment = .center
     ginvenbutton.setTitle(text, for: .normal)
             
    }
    func createParticlesOnView(containerV:UIView) {
           let particleEmitter = CAEmitterLayer()

           particleEmitter.emitterPosition = CGPoint(x: view.center.x, y: -96)
           particleEmitter.emitterShape = .line
           particleEmitter.emitterSize = CGSize(width: view.frame.size.width, height: 1)

           let red = makeEmitterCell(color: UIColor.red)
           let green = makeEmitterCell(color: UIColor.green)
           let blue = makeEmitterCell(color: UIColor.blue)

           particleEmitter.emitterCells = [red, green, blue]
        containerV.layer.insertSublayer(particleEmitter, at: 0)

//           containerV.layer.addSublayer(particleEmitter)
       }
    func makeEmitterCell(color: UIColor) -> CAEmitterCell {
        let cell = CAEmitterCell()
        cell.birthRate = 3
        cell.lifetime = 7.0
        cell.lifetimeRange = 0
        cell.color = color.cgColor
        cell.velocity = 200
        cell.velocityRange = 50
        cell.emissionLongitude = CGFloat.pi
        cell.emissionRange = CGFloat.pi / 4
        cell.spin = 2
        cell.spinRange = 3
        cell.scaleRange = 0.5
        cell.scaleSpeed = -0.05

        cell.contents = UIImage(named: "drop")?.cgImage
        return cell
    }
    func speakgivenSpeech(speech:String)  {
           let speechSynthesizer = AVSpeechSynthesizer()
             // Line 2. Create an instance of AVSpeechUtterance and pass in a String to be spoken.
             let speechUtterance: AVSpeechUtterance = AVSpeechUtterance(string: speech)
             //Line 3. Specify the speech utterance rate. 1 = speaking extremely the higher the values the slower speech patterns. The default rate, AVSpeechUtteranceDefaultSpeechRate is 0.5
             speechUtterance.rate = AVSpeechUtteranceMaximumSpeechRate / 4.0
             // Line 4. Specify the voice. It is explicitly set to English here, but it will use the device default if not specified.
                 speechUtterance.voice = AVSpeechSynthesisVoice(language: "en-US")
             // Line 5. Pass in the urrerance to the synthesize
        speechSynthesizer.speak(speechUtterance)

    }
    func setGradient(toView:UIView,withColor:[CGColor]) {
           let gradientLayer = CAGradientLayer()
           gradientLayer.frame = toView.bounds
           gradientLayer.colors = [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor]
           gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
           toView.layer.insertSublayer(gradientLayer, at: 0)
       }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
