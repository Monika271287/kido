import UIKit
import CoreGraphics
//shake animation: http://stackoverflow.com/questions/27987048/shake-animation-for-uitextfield-uiview-in-swift
public extension UIView {

    func shake(count : Float? = nil,for duration : TimeInterval? = nil,withTanslation translation : Float? = nil) {
    let animation : CABasicAnimation = CABasicAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)

    animation.repeatCount = count ?? 2
    animation.duration = (duration ?? 0.5)/TimeInterval(animation.repeatCount)
    animation.autoreverses = true
    animation.byValue = translation ?? -5
    layer.add(animation, forKey: "shake")
    }
    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
           let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
           rotateAnimation.fromValue = 0.0
           rotateAnimation.toValue = CGFloat(M_PI * 2.0)
           rotateAnimation.duration = duration
           
           if let delegate: AnyObject = completionDelegate {
               rotateAnimation.delegate = delegate as! CAAnimationDelegate
           }
           self.layer.add(rotateAnimation, forKey: nil)
       }
}



