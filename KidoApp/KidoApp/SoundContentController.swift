//
//  SoundContentController.swift
//  KidoApp
//
//  Created by monika kumari on 07/06/20.
//  Copyright © 2020 monika kumari. All rights reserved.
//

import UIKit
import Foundation
//Protocol Declaration
extension UIImage {

    public func maskWithColor(color: UIColor) -> UIImage {

        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let context = UIGraphicsGetCurrentContext()!

        let rect = CGRect(origin: CGPoint.zero, size: size)

        color.setFill()
        self.draw(in: rect)

        context.setBlendMode(.sourceIn)
        context.fill(rect)

        let resultImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return resultImage
    }

}
protocol SoundContentControllerDelegate:class {
    func popoverContent(controller:SoundContentController, didselectItem name:String)
}
//End Protocol
class PlayListCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var animalNameLabel: UILabel!
    @IBOutlet weak var animalImgV: UIImageView!

}
class SoundContentController: BaseViewController,UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var titleLabel: UILabel!

    var delegate:SoundContentControllerDelegate? //declare a delegate
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
     var items = ["ANIMALS", "BIRDS", "INSTRUMENTS", "ALPHABETS", "NUMBERS"]
     var items_image = ["animals", "birds", "instruments", "alphabets", "numbers"]
     @IBOutlet weak var collectionV: UICollectionView!
    @IBOutlet weak var closeButton: UIButton!


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)

    }
    override func viewDidLoad() {
          super.viewDidLoad()
        dLog(message: "")
        closeButton.setBackgroundImage(UIImage(named: "back")?.maskWithColor(color: UIColor.green), for: .normal)
       self.setGradient(toView: self.view, withColor: [UIColor.red.cgColor,    UIColor.green.cgColor, UIColor.blue.cgColor])
           self.collectionV.backgroundColor = UIColor.clear

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation
     

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    
    */
    // MARK: - UICollectionViewDataSource protocol
        func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
           return 1     //return number of sections in collection view
       }

           // tell the collection view how many cells to make
           func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
               return self.items.count
           }

           // make a cell for each cell index path
           func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

               // get a reference to our storyboard cell
               let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! PlayListCollectionViewCell

               // Use the outlet in our custom class to get a reference to the UILabel in the cell
               cell.animalImgV.image = UIImage(named: self.items_image[indexPath.item])
               cell.animalNameLabel.text = self.items[indexPath.item]
               cell.backgroundColor = UIColor.clear // make cell more visible in our example project

               return cell
           }

           // MARK: - UICollectionViewDelegate protocol

           func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
               // handle tap events
               print("You selected cell #\(indexPath.item)!")
               let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ListViewController") as? ListViewController
               vc?.selectedIndex = indexPath.row
           
                  self.dismiss(animated: true, completion: {
                    self.delegate?.popoverContent(controller: self, didselectItem: self.items[indexPath.row])

                  })
            

            
//                      self.navigationController?.pushViewController(vc!, animated: true)
            
                  
               
           }
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let screenWidth = UIScreen.main.bounds.width
              let scaleFactor = (screenWidth )
              
              return CGSize(width: scaleFactor, height: 240)
          }
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
           return UIEdgeInsets(top: 20,left: 0,bottom: 20,right: 0)
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
          
          func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
              return 0
          }
    @IBAction func closeButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
         }
   

}
